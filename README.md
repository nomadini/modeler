#Modeler

Creates SGD models from ad history data and persists them in cassandra
, those models are used by Scorer service.

### Internal details explanation
    SgdSolver.cpp                       is the main class that runs the SVMSGD classifier from shogun machine learning library
                                        
    
    ModelerApp.cpp                      is the main entry to the application
    ModelerAsyncJobService.cpp          runs the modeling piple line for different model setups that are created by users, every N minutes
    
    ModelerEngine package
                                        contains all the different modules that to validate featuers, clean the data, calculates feature weights and persist models 
    