#ifndef ModelerAsyncJobService_H
#define ModelerAsyncJobService_H


#include <memory>
#include <string>
#include <vector>
#include <set>
class FeatureFetcherService;
class MySqlModelService;
class ModelerPersistenceService;
class EntityToModuleStateStats;
#include "AtomicBoolean.h"
#include <tbb/concurrent_hash_map.h>
class Pixel;
#include "MySqlPixelService.h"
#include "Poco/NotificationQueue.h"
#include "Poco/ThreadPool.h"
#include "DateTimeMacro.h"
class NegativeFeatureFetcher;
class PositiveFeatureFetcher;
class ModelCacheService;
class DataReloadService;
class ModelResultCacheService;
class ModelDataCleaner;
class ModelerTask;
class DeviceGeoFeatureHistory;
class ModelRequest;
class EntityToModuleStateStatsPersistenceService;
class FeatureBasedModeler;
class ModelerAsyncJobService : public std::enable_shared_from_this<ModelerAsyncJobService> {

public:
TimeType lastTimeDataWasReloadedInSeconds;
gicapods::ConfigService* configService;
EntityToModuleStateStats* entityToModuleStateStats;
ModelCacheService* modelCacheService;
ModelResultCacheService* modelResultCacheService;
DataReloadService* dataReloadService;
std::shared_ptr<gicapods::AtomicBoolean> isMetricPersistenceInProgress;
EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;
std::unordered_map<std::string, std::set<std::string> > typePermutationNameMap;

ModelDataCleaner* modelDataCleaner;
NegativeFeatureFetcher* negativeFeatureFetcher;
PositiveFeatureFetcher* positiveFeatureFetcher;
ModelerPersistenceService* modelerPersistenceService;
FeatureBasedModeler* featureBasedModeler;

ModelerAsyncJobService();

bool modelerIsUpAndRunning;

virtual ~ModelerAsyncJobService();

void runEveryMinute();

void refreshData();

void runAsynchronousModeler();

void runModelingOnRequest();

void startOtherThreads();

private:

void populateRealIdIfExisiting(std::shared_ptr<ModelRequest> modelRequest);
std::vector<std::shared_ptr<ModelerTask > > createModelerTaskVariations(std::shared_ptr<ModelRequest> modelRequest);

std::vector<std::shared_ptr<ModelerTask > > createModelerTaskWithDifferentFeatureRecency(
        std::shared_ptr<ModelRequest> modelRequest,
        std::string description,
        std::string segmentSeedName,
        std::string modelName,
        std::set<std::string> featureTypesRequested);

std::shared_ptr<ModelerTask > createModelerTask(std::shared_ptr<ModelRequest> modelRequest);
void printTheOrderOfTasks(
        std::vector<std::shared_ptr<ModelerTask> > allModelsToRun);
void runModelerTasks(
        std::vector<std::shared_ptr<ModelerTask > >  allTasksForModelRequest);

};

#endif
