/*
 * ModelerAsyncJobService.cpp
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#include "GUtil.h"


#include "FeatureFetcherService.h"
#include "CollectionUtil.h"
#include "DataReloadService.h"
#include "ModelDataCleaner.h"
#include "SignalHandler.h"
#include <thread>
#include "ModelerAsyncJobService.h"
#include "EntityToModuleStateStats.h"
#include "ConfigService.h"
#include "Pixel.h"
#include "NegativeFeatureFetcher.h"
#include "PositiveFeatureFetcher.h"
#include "ModelerTask.h"
#include "ModelCacheService.h"
#include "ModelerPersistenceService.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "FeatureBasedModeler.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Net/ServerSocket.h"
#include "ConverterUtil.h"
#include <boost/exception/all.hpp>
#include "ModelerTestsHelper.h"
#include "ModelRequest.h"
#include <boost/foreach.hpp>
#include "HttpUtil.h"
#include "Pixel.h"
#include "ModelerContext.h"
#include "ModelResultCacheService.h"

#include "ModelerTask.h"
#include "ModelRequest.h"
#include "Feature.h"
#include "DeviceFeatureHistory.h"
#include "DateTimeUtil.h"

ModelerAsyncJobService::ModelerAsyncJobService(){
        lastTimeDataWasReloadedInSeconds = DateTimeUtil::getNowInSecond();

        std::vector<std::set<std::string> > featureTypePermutations = {
                {Feature::generalTopLevelDomain},
                {Feature::MGRS100},
                {Feature::PLACE_TAG},
                {Feature::generalTopLevelDomain, Feature::MGRS100},
                {Feature::generalTopLevelDomain, Feature::PLACE_TAG},
                {Feature::MGRS100, Feature::PLACE_TAG},
                {Feature::generalTopLevelDomain, Feature::MGRS100, Feature::PLACE_TAG}
        };

        typePermutationNameMap.insert(std::make_pair(Feature::generalTopLevelDomain,
                                                     featureTypePermutations.at(0)));

        typePermutationNameMap.insert(std::make_pair(Feature::MGRS100,
                                                     featureTypePermutations.at(1)));

        typePermutationNameMap.insert(std::make_pair(Feature::PLACE_TAG,
                                                     featureTypePermutations.at(2)));

        typePermutationNameMap.insert(std::make_pair(Feature::generalTopLevelDomain + "_"+ Feature::MGRS100,
                                                     featureTypePermutations.at(3)));

        typePermutationNameMap.insert(std::make_pair(Feature::generalTopLevelDomain + "_"+ Feature::PLACE_TAG,
                                                     featureTypePermutations.at(4)));

        typePermutationNameMap.insert(std::make_pair(Feature::MGRS100 + "_"+ Feature::PLACE_TAG,
                                                     featureTypePermutations.at(5)));

        typePermutationNameMap.insert(std::make_pair(Feature::generalTopLevelDomain + "_" + Feature::generalTopLevelDomain + "_"+ Feature::PLACE_TAG,
                                                     featureTypePermutations.at(6)));

}

ModelerAsyncJobService::~ModelerAsyncJobService() {

}


void ModelerAsyncJobService::runEveryMinute() {
        while (true) {
                try {


                        gicapods::Util::printMemoryUsage();
                        refreshData();
                        gicapods::Util::sleepViaBoost (_L_,
                                                       ConverterUtil::convertTo<int> (
                                                               configService->get ("asynchronousModelerIntervalInSeconds")));
                } catch(...) {
                        entityToModuleStateStats->addStateModuleForEntity(
                                "EXCEPTION_IN_RUN_EVERY_MINUTE",
                                "ModelerAsyncJobService",
                                EntityToModuleStateStats::all,
                                EntityToModuleStateStats::exception
                                );
                }
                int oldnessOfReloadedData = DateTimeUtil::getNowInSecond() - lastTimeDataWasReloadedInSeconds;
                if (oldnessOfReloadedData > 300) {
                        entityToModuleStateStats->addStateModuleForEntity(
                                "DATA_IS_TOO_OLD",
                                "ModelerAsyncJobService",
                                EntityToModuleStateStats::all,
                                EntityToModuleStateStats::exception
                                );
                }

        }
}


void ModelerAsyncJobService::refreshData() {
        dataReloadService->reloadDataViaHttp();
}

void ModelerAsyncJobService::runModelingOnRequest() {

        std::vector<std::shared_ptr<ModelRequest> > allModelsToRun;
        auto allModels = modelCacheService->getAllEntities();
        for (auto model : *allModels) {

                if (model->parentModelRequestId > 0) {
                        //we dont want to rebuild a model variation for now
                        MLOG(3)<< "ignoring model with id : "<<model->id << " since its a variation, will reconstruct variation later";
                        continue;
                }
                if (StringUtil::equalsIgnoreCase(
                            model->modelType, ModelRequest::actionTakerCatchAllModel)) {
                        MLOG(3)<< "ignoring model with id : "<<model->id << " since its a action taker dummy model";
                        continue;
                }


                LOG(INFO) << "going to buidling models for model : " << model->name << " with id "<< model->id;
                allModelsToRun.push_back(model);
        }

        LOG(INFO) << "buidling models for " << allModelsToRun.size ()<<" requests";

        std::vector<std::shared_ptr<ModelerTask> > allTasksForAllModelVariations;
        std::vector<std::shared_ptr<ModelerTask> > allTasksForModelRequest;

        for (std::shared_ptr<ModelRequest> modelRequest : allModelsToRun) {
                auto modelerTask = createModelerTask(modelRequest);
                allTasksForModelRequest.push_back(modelerTask);

                //we add the variations to a list
                auto modelerTaskVariations = createModelerTaskVariations(modelRequest);
                for (auto variation : modelerTaskVariations) {
                        allTasksForAllModelVariations.push_back(variation);
                }
        }

        printTheOrderOfTasks(allTasksForModelRequest);
        printTheOrderOfTasks(allTasksForAllModelVariations);

        runModelerTasks(allTasksForModelRequest);
        runModelerTasks(allTasksForAllModelVariations);
}


void ModelerAsyncJobService::printTheOrderOfTasks(
        std::vector<std::shared_ptr<ModelerTask> > allModelsToRun) {
        LOG(INFO)<<"Order Of Models To Build";
        for (auto modelerTask : allModelsToRun) {
                if (modelerTask->isEliggibleToRun()) {
                        LOG(INFO)<<"id : "
                                 << modelerTask->modelerContext->modelRequest->id
                                 <<" name : "
                                 << modelerTask->modelerContext->modelRequest->name;
                }
        }
}

void ModelerAsyncJobService::populateRealIdIfExisiting(
        std::shared_ptr<ModelRequest> modelRequest) {
        //if modeler is a variation , it's id is -1,
        //we look up the "name + advertiser_id" combo from a ModelCacheService (because it's unique key for a modelRequest)
        //and if there is any already. we get the real key
        auto existingModelRequest =
                modelCacheService->uniqueKeyToModelMap->getOptional(modelRequest->getUniqueKey());
        if (existingModelRequest != nullptr) {
                modelRequest->id = existingModelRequest->id;
                MLOG(3)<<"finding modelRequest for variation real id : "<< modelRequest->id;
        }
}

void ModelerAsyncJobService::runModelerTasks(
        std::vector<std::shared_ptr<ModelerTask > >  allTasksForModelRequest) {
        for (std::shared_ptr<ModelerTask> variation :  allTasksForModelRequest) {

                auto modelResult = variation->modelerContext->modelRequest->modelResult;
                auto startTime = DateTimeUtil::getNowInSecond();
                if (!variation->isEliggibleToRun()) {
                        LOG(INFO)<<"skipping already unEligibleToRun model"
                                 << variation->modelerContext->modelRequest->id
                                 << ", name : "
                                 << variation->modelerContext->modelRequest->name;
                        continue;

                }
                LOG(INFO) << "buidling models for model id :  "
                          << variation->modelerContext->modelRequest->id
                          << ", name : "
                          << variation->modelerContext->modelRequest->name;
                if (modelResult != nullptr) {
                        LOG(INFO) <<" previous modelResult : "
                                  << modelResult->toJson();
                }

                variation->run();
                LOG(INFO)<< "finished building modelerTask "<<
                        variation->modelerContext->modelRequest->name
                         <<" in "
                         <<DateTimeUtil::getNowInSecond() - startTime<<" seconds";
        }
}
std::vector<std::shared_ptr<ModelerTask > >
ModelerAsyncJobService::createModelerTaskWithDifferentFeatureRecency(
        std::shared_ptr<ModelRequest> modelRequest,
        std::string description,
        std::string segmentSeedName,
        std::string modelName,
        std::set<std::string> featureTypesRequested) {


        auto halfRecencyVariation = ModelRequest::clone(modelRequest);
        halfRecencyVariation->id = -1;
        halfRecencyVariation->name = modelName + "_half_recency";
        halfRecencyVariation->description = description + "\nfeatureRecencyInSecond cut down to half";
        halfRecencyVariation->featureRecencyInSecond = modelRequest->featureRecencyInSecond / 2;
        halfRecencyVariation->segmentSeedName = segmentSeedName + "_half_recency";
        halfRecencyVariation->parentModelRequestId = modelRequest->id;
        halfRecencyVariation->featureTypesRequested = featureTypesRequested;


        auto doubleRecencyVariation = ModelRequest::clone(modelRequest);
        doubleRecencyVariation->id = -1;
        doubleRecencyVariation->name = modelName + "_2x_recency";
        doubleRecencyVariation->description = description + "\nfeatureRecencyInSecond doubled";
        doubleRecencyVariation->featureRecencyInSecond = modelRequest->featureRecencyInSecond * 2;
        doubleRecencyVariation->segmentSeedName = segmentSeedName + "_double_recency";
        doubleRecencyVariation->parentModelRequestId = modelRequest->id;
        doubleRecencyVariation->featureTypesRequested = featureTypesRequested;

        auto tenTimesRecencyVariation = ModelRequest::clone(modelRequest);
        tenTimesRecencyVariation->id = -1;
        tenTimesRecencyVariation->name = modelName + "_10x_recency";
        tenTimesRecencyVariation->description = description + "\nfeatureRecencyInSecond ten times";
        tenTimesRecencyVariation->featureRecencyInSecond = modelRequest->featureRecencyInSecond * 10;
        tenTimesRecencyVariation->segmentSeedName = segmentSeedName + "_10x_recency";
        tenTimesRecencyVariation->parentModelRequestId = modelRequest->id;
        tenTimesRecencyVariation->featureTypesRequested = featureTypesRequested;

        auto halfRecencyVariationTask = createModelerTask(halfRecencyVariation);
        auto doubleRecencyVariationTask = createModelerTask(doubleRecencyVariation);
        auto tenTimesRecencyVariationTask = createModelerTask(tenTimesRecencyVariation);

        std::vector<std::shared_ptr<ModelerTask > > allVariations;
        allVariations.push_back(halfRecencyVariationTask);
        allVariations.push_back(doubleRecencyVariationTask);
        allVariations.push_back(tenTimesRecencyVariationTask);
        return allVariations;
}

std::vector<std::shared_ptr<ModelerTask > > ModelerAsyncJobService::createModelerTaskVariations(
        std::shared_ptr<ModelRequest> modelRequest) {
        std::vector<std::shared_ptr<ModelerTask > > allVariations;

        for (auto entry : typePermutationNameMap) {

                std::string description = modelRequest->description;
                std::string segmentSeedName = modelRequest->segmentSeedName;
                std::string modelName = modelRequest->name;


                description = description + "\nfeatureTypesRequested changed to " + entry.first;
                segmentSeedName = segmentSeedName + "_featureTypesRequested_" + entry.first;

                //changing name causes us to insert new model rather than updating parents
                modelName = modelName + "_featureTypesRequested_" + entry.first;

                auto allVariationsWithFeatureTypesWanted =
                        createModelerTaskWithDifferentFeatureRecency(
                                modelRequest,
                                description,
                                segmentSeedName,
                                modelName,
                                entry.second);

                //TODO : change these two features too
                // clone->maxNumberOfDeviceHistoryPerFeature = original->maxNumberOfDeviceHistoryPerFeature;                //
                // clone->maxNumberOfNegativeFeaturesToPick = original->maxNumberOfNegativeFeaturesToPick;

                for (auto var : allVariationsWithFeatureTypesWanted) {
                        allVariations.push_back(var);
                }
        }




        return allVariations;
}


std::shared_ptr<ModelerTask > ModelerAsyncJobService::createModelerTask(
        std::shared_ptr<ModelRequest> modelRequest) {
        //we populate real id, if existing
        populateRealIdIfExisiting(modelRequest);

        //populate modelResult if existing
        auto modelResult = modelResultCacheService->
                           modelRequestIdToResultMap->getOptional(modelRequest->id);

        if (modelResult != nullptr) {
                modelRequest->modelResult = modelResult;
        }
        auto modelerTask = std::make_shared<ModelerTask>();
        std::shared_ptr<ModelerContext> context = std::make_shared<ModelerContext>();
        modelerTask->featureBasedModeler = featureBasedModeler;

        modelerTask->modelerContext = context;
        modelerTask->modelerContext->modelRequest = modelRequest;
        modelerTask->entityToModuleStateStats = entityToModuleStateStats;

        modelerTask->modelerPersistenceService = modelerPersistenceService;
        modelerTask->positiveFeatureFetcher = positiveFeatureFetcher;
        modelerTask->negativeFeatureFetcher = negativeFeatureFetcher;
        modelerTask->modelDataCleaner = modelDataCleaner;
        modelerTask->initModules();


        return modelerTask;
}

void ModelerAsyncJobService::runAsynchronousModeler() {
        while (true) {
                try {


                        gicapods::Util::sleepViaBoost(_L_, 10);//wait for 10 seconds until modeler instance is up and running
                        runModelingOnRequest();

                        MLOG(3) << "going to start reading old modelling requests....";
                } catch (std::exception const &e) {
                        LOG(ERROR) << "error happening when handling request  " << boost::diagnostic_information (e);
                        LOG(ERROR) << "sending no bad request as a result of exception";
                        //  CounterService::getCounterService ()->count (
                        //          "numberOfExceptionsInHandlingStaleModelingRequest");
                }

                catch (...) {
                        LOG(ERROR) << "unknown error happening when handling request";
                        LOG(ERROR) << "sending no bad request as a result of exception";
                        //CounterService::getCounterService ()->count (
                        //        "numberOfExceptionsInHandlingStaleModelingRequest");
                }

                gicapods::Util::sleepViaBoost (_L_,
                                               ConverterUtil::convertTo<int> (
                                                       configService->get ("asynchronousModelerIntervalInSeconds")));

        }
}

void ModelerAsyncJobService::startOtherThreads() {

        MLOG(3) << "going to run daemon threads for modeler";
        std::thread everyMinuteThread (&ModelerAsyncJobService::runEveryMinute, this);
        everyMinuteThread.detach ();


        this->entityToModuleStateStatsPersistenceService->startThread();

}
