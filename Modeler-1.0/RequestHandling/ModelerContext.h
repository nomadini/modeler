#ifndef ModelerContext_H
#define ModelerContext_H



#include "Status.h"
class StringUtil;
#include <unordered_map>
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
class Pixel;
class DeviceFeatureHistory;
class ModelRequest;
class DeviceGeoFeatureHistory;

class ModelerContext {

public:

private:

public:

Poco::Net::HTTPServerRequest* httpRequestPtr;
Poco::Net::HTTPServerResponse* httpResponsePtr;
std::unordered_map<std::string, std::string> queryParams;
std::unordered_map<std::string, std::string> cookieMap;

std::shared_ptr<ModelRequest> modelRequest;
std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > allDeviceHistories;
std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > positiveDeviceHistories;
std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > negativeDeviceHistories;
std::vector<std::string> redFlags;
ModelerContext();
std::string toString();

static std::shared_ptr<ModelerContext> fromJson(std::string json);

std::string toJson();

virtual ~ModelerContext();

};

#endif
