
#include "ModelerContext.h"
#include "JsonUtil.h"
#include "GUtil.h"

#include "ModelRequest.h"
#include "DeviceFeatureHistory.h"


ModelerContext::ModelerContext() {
								allDeviceHistories = std::make_shared<std::vector<std::shared_ptr<DeviceFeatureHistory> > > ();
								positiveDeviceHistories = std::make_shared<std::vector<std::shared_ptr<DeviceFeatureHistory> > > ();
								negativeDeviceHistories = std::make_shared<std::vector<std::shared_ptr<DeviceFeatureHistory> > > ();
}


std::string ModelerContext::toString() {
								return this->toJson();
}



std::string ModelerContext::toJson() {

								return "";
}


ModelerContext::~ModelerContext() {

}
