/*
 * FeatureBasedModeler.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef SVMSGDMODELER_TESTS_H_
#define SVMSGDMODELER_TESTS_H_




class CassandraDriverInterface;
#include "ModelRequest.h"

namespace gicapods { class ConfigService; }
#include "ModelerTask.h"

class FeatureDeviceHistoryCassandraService;
#include "ModelRequest.h"
#include "ModelRequest.h"
#include "ModelerPersistenceService.h"
#include "ModelRequest.h"
#include "TestsCommon.h"
#include "ModelerTestsHelper.h"
class EntityToModuleStateStats;
MATCHER(IsPersistentDto, "isPersistentDto") {

        LOG(INFO)<<"typeid(arg_type) vs  typeid(std::shared_ptr<ModelRequest>) :   "<<typeid(arg).name()<<" vs "<<,
                typeid(std::shared_ptr<ModelRequest>).name();

        return (typeid(arg) == typeid(std::shared_ptr<ModelRequest>));

}

void verifyModelRequestValuesAreAsExpected(std::shared_ptr<ModelRequest> seedModelRequest,
                                           std::shared_ptr<ModelRequest> actualModelCreated) {

        EXPECT_THAT(actualModelCreated->offerId, testing::Eq(seedModelRequest->offerId));
        EXPECT_THAT(actualModelCreated->name, testing::Eq(seedModelRequest->name));
        EXPECT_THAT(actualModelCreated->segmentSeedName, testing::Eq(seedModelRequest->segmentSeedName));
        EXPECT_THAT(actualModelCreated->algorithmToModelBasedOn,testing::Eq(seedModelRequest->algorithmToModelBasedOn));
        EXPECT_THAT(actualModelCreated->dateOfRequest,testing::Eq(seedModelRequest->dateOfRequest));
        EXPECT_THAT(actualModelCreated->numberOfNegativeDevicesUsedForModeling, testing::Gt(0));
        EXPECT_THAT(actualModelCreated->numberOfPositiveDevicesUsed, testing::Gt(0));
        EXPECT_THAT(actualModelCreated->featureRecencyInSecond, testing::Gt(0));


}

TEST(VisitFeatureBasedModelerTest, CanCreateSeedModelAndPersist) {
        auto modelPersistenceService = std::make_shared<ModelerPersistenceServiceMock>();

        MLOG(3)<<"testCreatingModel";

//    EXPECT_CALL(*modelPersistenceService, persistModelInDataStores(
//       TypedEq<std::shared_ptr<ModelRequest>>(std::make_shared<ModelRequest>())));

        EXPECT_CALL(*modelPersistenceService,
                    persistModelInDataStores(IsPersistentDto()));


//   EXPECT_CALL(*modelPersistenceService, persistModelInDataStores(5));

        init_shogun();
        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();
        auto sgdSolver = std::make_shared<SgdSolver>();
        sgdSolver->entityToModuleStateStats = entityToModuleStateStats;

        std::shared_ptr<FeatureBasedModeler> featureBasedModeler(new FeatureBasedModeler(
                                                                              modelPersistenceService,
                                                                              sgdSolver
                                                                              )
                                                                      );
        featureBasedModeler->entityToModuleStateStats = entityToModuleStateStats;
        std::shared_ptr<DummyNegativeFeatureFetcher> mockNegativeFeatureFetcher = std::make_shared<DummyNegativeFeatureFetcher>();
        std::shared_ptr<DummyPositiveFeatureFetcher> mockPositiveFeatureFetcher = std::make_shared<DummyPositiveFeatureFetcher>();
        std::shared_ptr<ModelDataProvider> modelDataProviderLocal(new ModelDataProvider(
                                                                          std::move(mockNegativeFeatureFetcher),
                                                                          std::move(mockPositiveFeatureFetcher)));
        featureBasedModeler->setModelDataProvider(modelDataProviderLocal);
        auto seedModelRequest = ModelRequestTestHelper::createSeedModelRequest();
        auto actualModelCreated = featureBasedModeler->createAndPersistModel(StringUtil::random_string(12), seedModelRequest);

        //TODO : do the right asserts on
        verifyModelRequestValuesAreAsExpected(seedModelRequest, actualModelCreated);


//    MLOG(3)<<"actualModelCreated : "<<actualModelCreated->toJson();
//    MLOG(3)<<"end of testCreatingModel";

        Mock::VerifyAndClearExpectations(modelPersistenceService.get());

//EXPECT_THAT(Foo(), StartsWith("Hello"));
//EXPECT_THAT(Bar(), MatchesRegex("Line \\d+"));
//ASSERT_THAT(Baz(), AllOf(Ge(5), Le(10)));
//    EXPECT_CALL(*modelPersistenceService, persistModelInDataStores(std::make_shared<ModelRequest>()))
//      .Times(AtLeast(1)); //this compiles and works


//    EXPECT_TRUE(painter.DrawCircle(0, 0, 10));
}

#endif /* SVMSGDMODELER_TESTS_H_ */
