//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef FST30DemoTests_H
#define FST30DemoTests_H

#include "ModelerFunctionalTests_creatingOneModel.h"
class PixelDeviceHistoryCassandraService;

class FST30DemoTests : public ::testing::Test {
public:

void SetUp();

void TearDown();

FST30DemoTests();

int whenDemoRuns();
};

#endif //FST30DemoTests_H
