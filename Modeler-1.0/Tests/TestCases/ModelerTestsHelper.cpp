
#include "ModelerTestsHelper.h"
#include "DeviceFeatureHistory.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "Poco/AutoPtr.h"
#include "Poco/Notification.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "Device.h"
#include "RandomUtil.h"

std::shared_ptr<Offer> ModelerTestsHelper::getRandomOfferWithRandomPixel(int size) {
        std::vector<int> pixelIds;
        for(int i=0; i < size; i++) {
                pixelIds.push_back(i);
        }

        std::shared_ptr<Offer> off = std::make_shared<Offer>();
        off->advertiserId = RandomUtil::sudoRandomNumber(100);
        off->name=StringUtil::toStr("offerIdName").append(StringUtil::random_string(4));

        return off;

}

std::shared_ptr<PixelDeviceHistory> ModelerTestsHelper::createListOfPixelDeviceHistory(int size) {


        std::shared_ptr<PixelDeviceHistory> pixelDeviceHistory = std::make_shared<PixelDeviceHistory>();
        pixelDeviceHistory->pixelUniqueKey = StringUtil::toStr("pixel").append (StringUtil::random_string (4));

        auto devicesHittingPixel = std::make_shared<std::unordered_map <std::string, std::shared_ptr<Device> > >();

        for (int i = 0; i < size; i++) {//change after valgrind
                // std::shared_ptr<Device> devicePtr  = std::make_shared<Device> ();
                // devicePtr->deviceId = StringUtil::toStr("dev").append (StringUtil::random_string (4));
                // devicePtr->deviceTypeStr = StringUtil::toStr("MOBILE_PHONE");

                // devicesHittingPixel->insert (
                //         std::pair<std::string, std::shared_ptr<Device> > (
                //                 StringUtil::toStr(DateTimeUtil::getNowInMicroSecond ()),
                //                 devicePtr)
                //         );
        }

        pixelDeviceHistory->devicesHittingPixel = devicesHittingPixel;

        return pixelDeviceHistory;
}

std::shared_ptr<DeviceFeatureHistory> ModelerTestsHelper::createDeviceFeatureHistory(std::string prefix, int size) {

        std::shared_ptr<DeviceFeatureHistory> pos = std::make_shared<DeviceFeatureHistory>(
                std::make_shared<Device>(Device::createStandardDeviceId(), "DESKTOP"),
                Feature::generalTopLevelDomain);

        for(int i=0; i < size; i++ ) {
                // std::string domain = prefix.append(StringUtil::toStr("Domain").append(StringUtil::random_string(3)));
                // MLOG(3)<<"adding this domain to feature history: "<<domain;
                // pos->timeFeatureMap.insert(
                //         std::pair<TimeType, std::shared_ptr<Feature> >(
                //                 DateTimeUtil::getNowInMilliSecond(),
                //                 domain));


        }
        if(pos->timeFeatureMap.empty()) {
                throwEx("size of history is 0 ");
        }

        return pos;
}

std::vector<std::shared_ptr<DeviceFeatureHistory> > ModelerTestsHelper::createMockDeviceFeatureHistories
        (std::string prefix, int size) {

        std::vector<std::shared_ptr<DeviceFeatureHistory> > allDeviceFeatureHistories;

        for(int i=0; i < size; i++) {//change this after valgrind is done with testing the memory
                auto pos = ModelerTestsHelper::createDeviceFeatureHistory(prefix, size);
                allDeviceFeatureHistories.push_back(pos);

        }
        return allDeviceFeatureHistories;
}
