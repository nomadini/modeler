#include "DeviceFeatureHistoryCassandraService.h"
#include "DeviceFeatureHistoryTestHelper.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "FunctionUtil.h"
#include "ActionTakerCassandraService.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"

#include "CollectionUtil.h"
#include <boost/foreach.hpp>

void VisitFeatureBasedModelerTestHelper::insertActionData(std::string seedWebsite,
                                                          std::string offerId,
                                                          std::vector <std::shared_ptr<DeviceHistory> > actionTakersDevices,
                                                          DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService) {
        Poco::Timestamp now;
        MLOG(3) << "inserting action and non action data for offerId :  " << offerId << ", seed : " << seedWebsite;
        createActionTakersData(offerId);

        insertActionTakersDeviceFeatureHistory(offerId, seedWebsite,
                                               actionTakersDevices,
                                               deviceFeatureHistoryCassandraService);

        gicapods::Util::printTimeOfRunInSec(now);
}

/**
 * this will create device history for offer ids who are action takers
 */
void VisitFeatureBasedModelerTestHelper::insertActionTakersDeviceFeatureHistory(std::string offerId,
                                                                                std::string seedWebsite,
                                                                                std::vector <std::shared_ptr<DeviceHistory> > actionTakersDevices,
                                                                                DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService) {
        for (auto actionTaker : actionTakersDevices) {
                std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory =
                        DeviceFeatureHistoryTestHelper::createNewDeviceFeatureHistoryFromActionTakersPool(
                                seedWebsite);

                deviceFeatureHistory->device= std::make_shared<Device>(actionTaker->deviceId,
                                                                       actionTaker->getDeviceType());

                // deviceFeatureHistoryCassandraService->upsertDeviceFeatureHistory(bh);

        }
}

/**
 * this will create random device history for offer ids who are action takers
 */

void VisitFeatureBasedModelerTestHelper::insertNonActionTakersDeviceFeatureHistory(std::string offerId,
                                                                                   std::vector <std::shared_ptr<DeviceHistory> > nonActionTakersDevices,
                                                                                   DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService) {
        for (auto nonActionTaker : nonActionTakersDevices) {
                std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory =
                        DeviceFeatureHistoryTestHelper::createNewDeviceFeatureHistory();
                bh-device = std::make_shared<Device>(nonActionTaker->deviceId,
                                                     nonActionTaker->getDeviceType());

                // deviceFeatureHistoryCassandraService->upsertDeviceFeatureHistory(bh);
        }
}

/**
 * this will create fake action takers data for an offerid
 */
void VisitFeatureBasedModelerTestHelper::createActionTakersData(std::string offerId) {

        int NUMBER_OF_ACTION_TAKERS = 1000;
        auto configService = std::make_shared<gicapods::ConfigService>("modeler.properties", "common-test.properties");
        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();
        auto cassandraDriver = std::make_shared<CassandraDriver>(configService, entityToModuleStateStats);
        auto httpUtilService = std::make_shared<HttpUtilService>();

        ActionTakerCassandraServicePtr actionTakerCassandraService =
                std::make_shared<ActionTakerCassandraService>
                        (cassandraDriver,
                        httpUtilService,
                        entityToModuleStateStats);

        for (int i = 0; i < NUMBER_OF_ACTION_TAKERS; i++) {
                std::string deviceId = StringUtil::random_string(20);

                actionTakerCassandraService->saveActionTakersForOffer(offerId,
                                                                      deviceId,
                                                                      StringUtil::toStr(DateTimeUtil::getNowInMilliSecond()));
        }

}

void VisitFeatureBasedModelerTestHelper::insertRandomFeatureDeviceHistory(int negativeFeaturesSize,
                                                                          DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService,
                                                                          std::shared_ptr<FeatureDeviceHistoryCassandraService> featureDeviceHistoryCassandraService) {

        for (int i = 0; i < negativeFeaturesSize; i++) {

                std::string randomWebsite = StringUtil::toStr("randomFeature-")
                                            + StringUtil::random_string(2);

                //inserting negative devices for this feature
                std::vector <std::shared_ptr<DeviceHistory> > negativeDevices =
                        VisitFeatureBasedModelerTestHelper::insertFeatureDeviceHistory(CollectionUtil::convertToList(randomWebsite), 5,
                                                                                       deviceFeatureHistoryCassandraService,
                                                                                       featureDeviceHistoryCassandraService);

                //now inserting history for the negative devices.
                for (auto device : negativeDevices) {
                        std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory =
                                DeviceFeatureHistoryTestHelper::createNewDeviceFeatureHistory();
                        deviceFeatureHistory->device = device;

                        // deviceFeatureHistoryCassandraService->upsertDeviceFeatureHistory(bh);
                }
        }
}

std::vector <std::shared_ptr<DeviceHistory> > VisitFeatureBasedModelerTestHelper::insertFeatureDeviceHistory(
        std::vector <std::string> seedWebsites, int numberOfDevices,
        DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService,
        std::shared_ptr<FeatureDeviceHistoryCassandraService> featureDeviceHistoryCassandraService) {
        assertAndThrow(!seedWebsites.empty());
        assertAndThrow(numberOfDevices > 0);

        std::vector <std::shared_ptr<DeviceHistory> > allDevices;
        std::vector <std::string> allDevicesWrittenInJson;
        for (int i = 0; i < numberOfDevices; i++) {
                auto deviceId = StringUtil::toStr("deviceId").append(StringUtil::toStr(i));
                deviceId = deviceId.append(StringUtil::toStr("_").append(StringUtil::random_string(2)));
                auto device = std::make_shared<Device>(deviceId, "");
                std::shared_ptr<DeviceHistory> devHistory = std::make_shared<DeviceHistory>(device);

                devHistory->timeOfVisit = DateTimeUtil::getNowInMicroSecond();
                allDevices.push_back(devHistory);
                allDevicesWrittenInJson.push_back(devHistory->toJson());
                auto featureDeviceHistory = std::make_shared<FeatureDeviceHistory>(
                        std::make_unique<Feature>(seedWebsite, 0)
                        );

                featureDeviceHistory->devicehistories.push_back(devHistory);
                BOOST_FOREACH(std::string
                              seedWebsite, seedWebsites) {
                        featureDeviceHistoryCassandraService->pushToWriteBatchQueue(featureDeviceHistory);
                }
        }
        MLOG(3) << "done adding  " << JsonArrayUtil::convertListToJson(allDevicesWrittenInJson) << " devices :  "
                << JsonArrayUtil::convertListToJson(seedWebsites) << " to this feature : " << numberOfDevices;

        //now inserting history for the positive devices.
        for (auto device : allDevices) {

                BOOST_FOREACH(std::string
                              seedWebsite, seedWebsites) {
                        std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory =
                                DeviceFeatureHistoryTestHelper::createNewDeviceFeatureHistoryFromActionTakersPool(seedWebsite);
                        deviceFeatureHistory->device = device;
                        // deviceFeatureHistoryCassandraService->upsertDeviceFeatureHistory(bh);
                }

        }
        return allDevices;
}
