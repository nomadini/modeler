//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef ARFFFileCreatorTests_H
#define ARFFFileCreatorTests_H

#include "ModelerFunctionalTests_creatingOneModel.h"
#include "TestBase.h"
#include "ModelDataProvider.h"

class ARFFFileCreatorTests : public TestBase {
public:
std::shared_ptr<ModelDataProvider> modelDataProvider;
void SetUp();

void TearDown();

ARFFFileCreatorTests();

};

#endif //ARFFFileCreatorTests_H
