/*
 * VisitFeatureBasedModelerTest.cpp
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#ifndef VisitFeatureBasedModelerTestHelper_H
#define VisitFeatureBasedModelerTestHelper_H


class DeviceHistory;
#include <memory>
#include <string>
#include <vector>

class FeatureDeviceHistoryCassandraService;

class DeviceFeatureHistoryCassandraService;

class VisitFeatureBasedModelerTestHelper {

public:

DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService;
std::shared_ptr<FeatureDeviceHistoryCassandraService> featureDeviceHistoryCassandraService;

/**
 * this function creates action taker
 * device histories based on a seedWebsite
 */
static void insertActionData(std::string seedWebsite,
                             std::string offerId,
                             std::vector <std::shared_ptr<DeviceHistory> > actionTakersDevices,
                             DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService);

/**
 * this will create device history for offer ids who are action takers
 */
static void insertActionTakersDeviceFeatureHistory(std::string offerId,
                                                   std::string seedWebsite,
                                                   std::vector <std::shared_ptr<DeviceHistory> > actionTakersDevices,
                                                   DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService);
/**
 * this will create random device history for offer ids who are action takers
 */

static void insertNonActionTakersDeviceFeatureHistory(std::string offerId,
                                                      std::vector <std::shared_ptr<DeviceHistory> > nonActionTakersDevices,
                                                      DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService);

/**
 * this will create fake action takers data for an offerid
 */
static void createActionTakersData(std::string offerId);

static void insertRandomFeatureDeviceHistory(int negativeFeaturesSize,
                                             DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService,
                                             std::shared_ptr<FeatureDeviceHistoryCassandraService> featureDeviceHistoryCassandraService);

static std::vector <std::shared_ptr<DeviceHistory> > insertFeatureDeviceHistory(
        std::vector <std::string> seedWebsites, int numberOfDevices,
        DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService,
        std::shared_ptr<FeatureDeviceHistoryCassandraService> featureDeviceHistoryCassandraService);
};

#endif
