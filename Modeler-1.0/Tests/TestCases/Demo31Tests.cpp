#include "Demo31Tests.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlDriver.h"
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "OfferTestHelper.h"

#include "Demo31.h"
#include "ARFFfileCreator.h"
#include "FeatureFetcherService.h"
#include "NegativeFeatureFetcher.h"
#include "PositiveFeatureFetcher.h"
#include "ModelDataProvider.h"
#include "ModelerFunctionalTestsHelper.h"
#include "FileUtil.h"

Demo31Tests::Demo31Tests() {
        demo = std::make_shared<Demo31>();
}

void Demo31Tests::SetUp() {
        TestBase::SetUp();


        std::shared_ptr<FeatureFetcherService<DeviceFeatureHistory> > featureFetcherService
                = std::make_shared<FeatureFetcherService<DeviceFeatureHistory> >(
                pixelDeviceHistoryCassandraService,
                ModelerFunctionalTestsHelper::offerCacheService(),
                deviceFeatureHistoryCassandraService,
                featureDeviceHistoryCassandraService);

        std::shared_ptr<NegativeFeatureFetcher> negativeFeatureFetcher(std::make_shared<NegativeFeatureFetcher>());
        std::shared_ptr<PositiveFeatureFetcher> positiveFeatureFetcher(std::make_shared<PositiveFeatureFetcher>(featureFetcherService));

        modelDataProvider = std::make_shared<ModelDataProvider>(negativeFeatureFetcher, positiveFeatureFetcher);
}

void Demo31Tests::TearDown() {
        TestBase::TearDown();
}

int Demo31Tests::whenDemoRuns() {
        return demo->runDemo();
}

std::string Demo31Tests::givenFileExistsBasedOnOurDataInCassandra(){
        auto seedModelRequest = givenASeedModelRequest ();

        givenDeviceHistoryInCassandraForSeedWebsites (
                seedModelRequest->seedWebsites, deviceFeatureHistoryCassandraService,featureDeviceHistoryCassandraService );

        std::vector<std::shared_ptr<DeviceFeatureHistory> > deviceFeatureHistories= modelDataProvider->provideDeviceHistoriesBasedOnSeedModel(seedModelRequest);

        std::set<int> orderedListOfFeaturesOutput;
        std::set<std::string> orderedListOfClassesOutput;


        std::shared_ptr<ARFFFileCreator> aRFFFileCreator= std::make_shared<ARFFFileCreator>(modelDataProvider);

        std::vector<std::shared_ptr<ARFFFRecord> > records = aRFFFileCreator->createRecordsFromDeviceFeatureHistories(
                deviceFeatureHistories,
                orderedListOfFeaturesOutput,
                orderedListOfClassesOutput);

        std::string fileName = "/tmp/mySample_" +StringUtil::random_string (4) + ".arff";
        aRFFFileCreator->createFile(fileName, records, orderedListOfFeaturesOutput, orderedListOfClassesOutput);
        return fileName;
}

TEST_F(Demo31Tests, runDemo31) {
        whenDemoRuns();
}

TEST_F(Demo31Tests, runDemo31WithOurData) {
        fileName = givenFileExistsBasedOnOurDataInCassandra();
        demo->runDemoWithFileName(fileName, 5);
        // FileUtil::deleteFile(fileName);
}
