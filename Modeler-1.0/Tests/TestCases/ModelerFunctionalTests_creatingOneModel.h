//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef GICAPODS_MODELERFUNCTIONALTESTS_H_H
#define GICAPODS_MODELERFUNCTIONALTESTS_H_H

#include "ModelRequest.h"

namespace gicapods { class ConfigService; }
#include "ModelerTask.h"

class FeatureDeviceHistoryCassandraService;
#include "ModelRequest.h"
#include "ModelerPersistenceService.h"

#include "ModelerTestsHelper.h"


class DeviceHistory;

#include "ModelerFunctionalTestsHelper.h"
#include "ModelUtil.h"
#include "MySqlModelService.h"
#include "MySqlOfferService.h"
#include "TestBase.h"

class ModelerFunctionalTests_creatingOneModel : public TestBase {


public:

    ModelerFunctionalTests_creatingOneModel();

    void SetUp();

    void TearDown();

    ~ModelerFunctionalTests_creatingOneModel();

    static std::shared_ptr<Offer> givenDataOfferDataInMySql();

    static std::vector <std::shared_ptr<PixelDeviceHistory>> insertPixelDeviceHistory(std::shared_ptr<Offer> offer,
                                                                        PixelDeviceHistoryCassandraService* pixelDeviceHistoryCassandraService);

    static void givenFeatureAndDeviceHistoryInCassandra(std::shared_ptr<Offer> offer, PixelDeviceHistoryCassandraService* pixelDeviceHistoryCassandraService,
                                                        DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService);

    std::vector <std::shared_ptr<ModelRequest>> whenRequestHitsTheModelerService(std::string jsonRequest);

    static void thenModelCreatedIsAsExpected(std::shared_ptr<ModelRequest> actual, std::shared_ptr<ModelRequest> expected,
                                      std::string modelType);

    static void thenModelExistsInDb(std::shared_ptr<ModelRequest> expected);

    std::shared_ptr<ModelRequest> givenAPixelModelRequest(std::shared_ptr<Offer> offer);

    static void thenTestWaitsUntilModelingIsDone();

};

#endif //GICAPODS_MODELERFUNCTIONALTESTS_H_H
