//
// Created by User on 6/5/2016.
//

#include "TestBase.h"
#include "ModelerFunctionalTestsHelper.h"

#include "ModelRequestTestHelper.h"
TestBase::TestBase() {
        auto configService = std::make_shared<gicapods::ConfigService>(
                "modeler-test.properties", "common-test.properties");

        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();
        auto cassandraDriver = std::make_shared<CassandraDriver>(configService, entityToModuleStateStats);

        auto httpUtilService = std::make_shared<HttpUtilService>();
        deviceFeatureHistoryCassandraService = std::make_shared<DeviceFeatureHistoryCassandraService>
                                                       (cassandraDriver,
                                                       httpUtilService,
                                                       entityToModuleStateStats);
        featureDeviceHistoryCassandraService =
                std::make_shared<FeatureDeviceHistoryCassandraService>(cassandraDriver,
                                                                       httpUtilService,
                                                                       entityToModuleStateStats);
        pixelDeviceHistoryCassandraService = std::make_shared<PixelDeviceHistoryCassandraService>(cassandraDriver,
                                                                                                  httpUtilService,
                                                                                                  entityToModuleStateStats);
}

void TestBase::SetUp() {
        // code here will execute just before the test ensues
        /** truncate all the device histories */
        auto configService = std::make_shared<gicapods::ConfigService>(
                "modeler-test.properties", "common-test.properties");
        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();
        auto cassandraDriver = std::make_shared<CassandraDriver>(configService, entityToModuleStateStats);

        deviceFeatureHistoryCassandraService->deleteAll ();
        featureDeviceHistoryCassandraService->deleteAll ();
        mysqlOfferService->deleteAllOffersAndAssociatedPixelsAndModels ();
        mySqlModelService->deleteAll ();
        /** truncate all the device histories */
}

void TestBase::TearDown() {
        // code here will be called just after the test completes
        // ok to through exceptions from here if need be

        mySqlOfferService->deleteAllOffersAndAssociatedPixelsAndModels();
        mySqlModelService->deleteAll();

        MLOG(3) << "going to sleep to  let the threads break out of their loops....";
        gicapods::Util::sleepViaBoost(_L_, 5);


}


void TestBase::givenDeviceHistoryInCassandraForSeedWebsites(
        std::vector<std::string> seedWebsites, DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService,
        std::shared_ptr<FeatureDeviceHistoryCassandraService> featureDeviceHistoryCassandraService) {
        std::vector<std::shared_ptr<DeviceHistory> > actionTakersDevices =
                VisitFeatureBasedModelerTestHelper::insertFeatureDeviceHistory (seedWebsites, 3, deviceFeatureHistoryCassandraService,
                                                                                featureDeviceHistoryCassandraService);
        int negativeFeaturesSize = 20;
        VisitFeatureBasedModelerTestHelper::insertRandomFeatureDeviceHistory (
                negativeFeaturesSize, deviceFeatureHistoryCassandraService,
                featureDeviceHistoryCassandraService);

        gicapods::Util::sleepViaBoost (_L_, 1);
}


std::shared_ptr<ModelRequest> TestBase::givenASeedModelRequest() {
        auto seedModelRequest = ModelRequestTestHelper::createSeedModelRequest ();
        return seedModelRequest;
}
