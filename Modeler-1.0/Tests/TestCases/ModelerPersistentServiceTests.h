/*
 * FeatureBasedModeler.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef MODELER_PERSISTENT_SERVICE_TESTS_H_
#define MODELER_PERSISTENT_SERVICE_TESTS_H_




class CassandraDriverInterface;
#include "ModelRequest.h"

namespace gicapods { class ConfigService; }
#include "ModelerTask.h"

class FeatureDeviceHistoryCassandraService;
#include "ModelRequest.h"
#include "ModelRequest.h"
#include "ModelerPersistenceService.h"
#include "ModelRequest.h"
#include "ModelerTestsHelper.h"
#include "TestsCommon.h"
#include "ModelRequestTestHelper.h"
class EntityToModuleStateStats;
MATCHER(IsPersistentDto, "isPersistentDto") {

        LOG(INFO)<<"typeid(arg_type) vs  typeid(std::shared_ptr<ModelRequest>) :   "<<typeid(arg).name()<<" vs "
                 <<typeid(std::shared_ptr<ModelRequest>).name();

        return (typeid(arg) == typeid(std::shared_ptr<ModelRequest>));

}


TEST(ModelerPersistentService, CanPersistModel) {
        //auto modelPersistenceService = std::make_shared<ModelerPersistenceServiceMock>();

        MLOG(3)<<"CanPersistModel";

//    EXPECT_CALL(*modelPersistenceService, persistModelInDataStores(
//       TypedEq<std::shared_ptr<ModelRequest>>(std::make_shared<ModelRequest>())));

//    EXPECT_CALL(*modelPersistenceService,
//                persistModelInDataStores(IsPersistentDto()));


//   EXPECT_CALL(*modelPersistenceService, persistModelInDataStores(5));

        init_shogun();
        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();
        auto sgdSolver = std::make_shared<SgdSolver>();
        sgdSolver->entityToModuleStateStats = entityToModuleStateStats;

        std::shared_ptr<FeatureBasedModeler> featureBasedModeler(new FeatureBasedModeler(
                                                             modelPersistenceService,
                                                             sgdSolver
                                                             )
                                                     );
        featureBasedModeler->entityToModuleStateStats = entityToModuleStateStats;
        std::shared_ptr<DummyNegativeFeatureFetcher> mockNegativeFeatureFetcher = std::make_shared<DummyNegativeFeatureFetcher>();
        std::shared_ptr<DummyPositiveFeatureFetcher> mockPositiveFeatureFetcher = std::make_shared<DummyPositiveFeatureFetcher>();
        std::shared_ptr<ModelDataProvider> modelDataProviderLocal(new ModelDataProvider(
                                                                          std::move(mockNegativeFeatureFetcher),
                                                                          std::move(mockPositiveFeatureFetcher)));
        featureBasedModeler->setModelDataProvider(modelDataProviderLocal);
        auto seedModelRequest = ModelRequestTestHelper::createSeedModelRequest();
        featureBasedModeler->createAndPersistModel(StringUtil::random_string(12), seedModelRequest);
        //TODO : do the right asserts on

        MLOG(3)<<"end of testCreatingModel";

        Mock::VerifyAndClearExpectations(modelPersistenceService.get());

//    EXPECT_CALL(*modelPersistenceService, persistModelInDataStores(std::make_shared<ModelRequest>()))
//      .Times(AtLeast(1)); //this compiles and works


//    EXPECT_TRUE(painter.DrawCircle(0, 0, 10));
}

#endif /* MODELER_PERSISTENT_SERVICE_TESTS_H_ */
