//
// Created by User on 6/5/2016.
//

#ifndef MODELER_TESTBASE_H
#define MODELER_TESTBASE_H

class PixelDeviceHistoryCassandraService;
#include "TestsCommon.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
class PixelDeviceHistoryCassandraService;

class CassandraDriverInterface;
#include <vector>
#include "ModelRequest.h"
class TestBase : public ::testing::Test {

public:

DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService;
std::shared_ptr<FeatureDeviceHistoryCassandraService> featureDeviceHistoryCassandraService;
PixelDeviceHistoryCassandraService* pixelDeviceHistoryCassandraService;

TestBase();

void SetUp();

void TearDown();

std::shared_ptr<ModelRequest> givenASeedModelRequest();

void givenDeviceHistoryInCassandraForSeedWebsites(std::vector <std::string> seedWebsites,
                                                  DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService,
                                                  std::shared_ptr<FeatureDeviceHistoryCassandraService> featureDeviceHistoryCassandraService );
};

#endif //MODELER_TESTBASE_H
