/*
 * FeatureBasedModeler.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef SVMSGD_WORKER_TESTS_H_
#define SVMSGD_WORKER_TESTS_H_




class CassandraDriverInterface;
#include "ModelRequest.h"

namespace gicapods { class ConfigService; }
#include "ModelerTask.h"

#include "FeatureDeviceHistoryCassandraService.h"
#include "ModelRequest.h"
#include "ModelRequest.h"
#include "ModelRequest.h"
#include "ModelerPersistenceService.h"
#include "ModelRequest.h"
#include "ModelerTestsHelper.h"
#include "TestsCommon.h"
#include "PixelDeviceHistoryCassandraServiceMock.h"
#include "MySqlDriverMock.h"
#include "MySqlOfferServiceMock.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "ModelerPersistenceService.h"
class EntityToModuleStateStats;

TEST(ModelerTaskTest, startThreadPoolAndPushAndBuildModel) {
        init_shogun();

        auto modelerPersistenceServiceMock = std::make_shared<NiceMock<ModelerPersistenceServiceMock> >();
        auto featureDeviceHistoryCassandraServiceMock = std::make_shared<NiceMock<FeatureDeviceHistoryCassandraServiceMock<FeatureDeviceHistory> > >();
        auto deviceFeatureHistoryCassandraServiceMock = std::make_shared<NiceMock<DeviceFeatureHistoryCassandraServiceMock<DeviceFeatureHistory> > >();

        auto pixelDeviceHistoryCassandraServiceMock = std::make_shared<NiceMock<PixelDeviceHistoryCassandraServiceMock> >();

        auto offerCacheServiceMock = std::make_shared<NiceMock<OfferCacheServiceMock> >();


        std::shared_ptr<PixelDeviceHistory> pixelDeviceHistory  = ModelerTestsHelper::createListOfPixelDeviceHistory(2);
        OfferPtr offer = ModelerTestsHelper::getRandomOfferWithRandomPixel(2);



        ON_CALL(*pixelDeviceHistoryCassandraServiceMock,
                readDeviceHistoryOfPixel(_,_))
        .WillByDefault(Return(pixelDeviceHistory));

        ON_CALL(*offerCacheServiceMock,
                findOfferById(_))
        .WillByDefault(Return(offer));

        std::vector<std::shared_ptr<DeviceFeatureHistory> > allDeviceFeatureHistories =
                ModelerTestsHelper::createMockDeviceFeatureHistories("positive", 4);

        ON_CALL(*deviceFeatureHistoryCassandraServiceMock,
                readFeatureHistoryOfDevice(_,_,_)).
        WillByDefault(Return(allDeviceFeatureHistories.at(0)));

        std::shared_ptr<FeatureFetcherService<DeviceFeatureHistory> > featureFetcherService =
                std::make_shared<FeatureFetcherService<DeviceFeatureHistory> >(
                        pixelDeviceHistoryCassandraServiceMock,
                        offerCacheServiceMock,
                        deviceFeatureHistoryCassandraServiceMock,
                        featureDeviceHistoryCassandraServiceMock
                        );

        std::shared_ptr<ModelRequest> modelRequest = std::dynamic_pointer_cast<ModelRequest>(ModelerTestsHelper::createPixelModelRequest());
        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();

        ModelerTask* modelerTask = new ModelerTask(featureFetcherService,
                                                   modelerPersistenceServiceMock,
                                                   modelRequest);
        modelerTask->entityToModuleStateStats = entityToModuleStateStats;
        // putRequestInQueue(modelRequest, featureFetcherService, modelerTask);

        // EXPECT_THAT(ModelerTask::notificationQueue->empty(), Not(true));


        Mock::VerifyAndClearExpectations(pixelDeviceHistoryCassandraServiceMock.get());
        MLOG(3)<<"end of sgd worker tests";


//    EXPECT_CALL(*modelerPersistenceServiceMock,
//                persistModelInDataStores(IsPersistentDto()));




//    auto modelPersistenceService = std::make_shared<ModelerPersistenceServiceMock>();
//
//    MLOG(3)<<"testCreatingModel";
//
////    EXPECT_CALL(*modelPersistenceService, persistModelInDataStores(
////       TypedEq<std::shared_ptr<ModelRequest>>(std::make_shared<ModelRequest>())));
//

//
//
////   EXPECT_CALL(*modelPersistenceService, persistModelInDataStores(5));
//
//    init_shogun();
//    std::shared_ptr<FeatureBasedModeler> featureBasedModeler(new FeatureBasedModeler(
//            modelPersistenceService,
//            std::make_shared<SgdSolver>()
//        )
//    );
//
//    std::shared_ptr<DummyNegativeFeatureFetcher> mockNegativeFeatureFetcher = std::make_shared<DummyNegativeFeatureFetcher>();
//    std::shared_ptr<DummyPositiveFeatureFetcher> mockPositiveFeatureFetcher = std::make_shared<DummyPositiveFeatureFetcher>();
//    std::shared_ptr<ModelDataProvider> modelDataProviderLocal(new ModelDataProvider(
//                                                                    std::move(mockNegativeFeatureFetcher),
//                                                                    std::move(mockPositiveFeatureFetcher)));
//    featureBasedModeler->setModelDataProvider(modelDataProviderLocal);
//    auto seedModelRequest = ModelerTestsHelper::createSeedModelRequest();
//    auto actualModelCreated = featureBasedModeler->createAndPersistModel(StringUtil::random_string(12), seedModelRequest);
//
//    //TODO : do the right asserts on
//    verifyModelRequestValuesAreAsExpected(seedModelRequest, actualModelCreated);
//
//
////    MLOG(3)<<"actualModelCreated : "<<actualModelCreated->toJson();
////    MLOG(3)<<"end of testCreatingModel";
//
//    Mock::VerifyAndClearExpectations(modelPersistenceService.get());

//EXPECT_THAT(Foo(), StartsWith("Hello"));
//EXPECT_THAT(Bar(), MatchesRegex("Line \\d+"));
//ASSERT_THAT(Baz(), AllOf(Ge(5), Le(10)));
//    EXPECT_CALL(*modelPersistenceService, persistModelInDataStores(std::make_shared<ModelRequest>()))
//      .Times(AtLeast(1)); //this compiles and works


//    EXPECT_TRUE(painter.DrawCircle(0, 0, 10));
}

#endif /* SVMSGD_WORKER_TESTS_H_ */
