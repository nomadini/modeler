//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef CreatingModelFromSubmittedRequestInDB_H
#define CreatingModelFromSubmittedRequestInDB_H

#include "ModelerFunctionalTests_creatingOneModel.h"
#include "TestBase.h"

class MySqlModelService;

class CreatingModelFromSubmittedRequestInDB : public TestBase {
public:

void SetUp();

void TearDown();

MySqlModelService* mySqlModelService;
CreatingModelFromSubmittedRequestInDB();

std::shared_ptr<ModelRequest> givenAModelRequestWithPositiveOffers(std::shared_ptr<Offer> offer);

std::shared_ptr<ModelRequest> givenAModelRequestWithSeedWebsites();

void givenTheRequestIsSubmittedInMySql(std::shared_ptr<ModelRequest> modelPersistentDto);

void whenAsynchronousModelerThreadIsRun();

void thenRequestIsPickedUpAndModelIsBuiltAsExpected(std::shared_ptr<ModelRequest> modelPersistentDtoPtr,
                                                    std::string modelType);
};

#endif //CreatingModelFromSubmittedRequestInDB_H
