#include "FST30DemoTests.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlDriver.h"
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "OfferTestHelper.h"

#include "FST30Demo.h"


FST30DemoTests::FST30DemoTests() {

}

void FST30DemoTests::SetUp() {

}

void FST30DemoTests::TearDown() {

}

int FST30DemoTests::whenDemoRuns() {
        std::shared_ptr<FST30Demo> demo30 = std::make_shared<FST30Demo>();
        demo30->runDemo();
}

TEST_F(FST30DemoTests, runDemo30) {
        whenDemoRuns();
}
