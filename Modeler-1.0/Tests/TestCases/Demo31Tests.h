//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef Demo31Tests_H
#define Demo31Tests_H

#include "ModelerFunctionalTests_creatingOneModel.h"
class PixelDeviceHistoryCassandraService;
#include "TestBase.h"
#include "ModelDataProvider.h"
#include "Demo31.h"

class Demo31Tests : public TestBase {
public:

std::shared_ptr<ModelDataProvider> modelDataProvider;
std::string fileName;
std::shared_ptr<Demo31> demo;
void SetUp();

void TearDown();

Demo31Tests();

int whenDemoRuns();

std::string givenFileExistsBasedOnOurDataInCassandra();
};

#endif //Demo31Tests_H
