#include "ModelerFunctionalTestsHelper.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>

int ModelerFunctionalTestsHelper::argc;

char **ModelerFunctionalTestsHelper::argv;

void ModelerFunctionalTestsHelper::init(int argc, char *argv[]) {
        ModelerFunctionalTestsHelper::argc = argc;
        ModelerFunctionalTestsHelper::argv = argv;
}
