///*
// * Modeler.cpp
// *
// *  Created on: Sep 7, 2015
// *      Author: mtaabodi
// */
//
//
//#include <Util/GUtil.h>
//
//
//#include "CassandraDriverInterface.h"
//#include "ModelRequest.h"
//#include "ConfigService.h"
//#include "ModelerTask.h"
//
//#include "FeatureDeviceHistoryCassandraService.h"
//#include "ModelRequest.h"
//#include <Galaxie/Modeler/Modeler-1.0/Utils/GalaxieSetup.h>
//class ModelerTestsHelper {
//
//public:
//
//	std::shared_ptr<ModelRequest> createModelRequest() {
//
//		std::shared_ptr<ModelRequest> req(new ModelRequest());
//
//		req->seedWebsites.insert(StringUtil::toStr("www.cnn.com"));
//		req->segmentSeedName = "cnn-visitor-today";
//		req->numberOfNegativeDevicesUsedForModeling = 100;
//		req->numberOfPositiveDevicesUsed = 100;
//		req->featureRecencyInSecond = 3600 * 24;
//
//		return req;
//	}
//
//	void insertSomePositiveAndNegativeDevicesForFeature(std::shared_ptr<ModelRequest> req) {
//		//we add some list of devices to this feature's deviceHistory
//		std::vector<std::shared_ptr<DeviceHistory>> actionTakersDevices =
//				VisitFeatureBasedModelerTestHelper::insertFeatureDeviceHistory(
//						req->seedWebsites, 100);
//		int negativeFeaturesSize = 100;
//		VisitFeatureBasedModelerTestHelper::insertRandomFeatureDeviceHistory(
//				negativeFeaturesSize);
//	}
//
//	void sendModelRequest() {
//		//creating the model request
//		auto req = createModelRequest();
//
//		//insertSomePositiveAndNegativeDevicesForFeature(req);
//
//		sendRequestToModeler(req);
//	}
//
//	bool positiveHistoryIsEmpty(std::string seedWebsite) {
//		std::vector<std::shared_ptr<FeatureDeviceHistory>> positiveBrowserHistories =
//				FeatureDeviceHistoryCassandraService::readDeviceHistoryOfFeatures(1, (TimeType) 0,
//						CollectionUtil::convertToList<std::string>(
//								seedWebsite));
//		MLOG(3)<<" "<<positiveBrowserHistories.size()<<" has visited this feature :  "<<seedWebsite<<" after time :";
//
//		return positiveBrowserHistories.size() == 0;
//	}
//
//	std::string sendRequestToModeler(std::shared_ptr<ModelRequest> modelRequest) {
//
//		MLOG(3)<<"sending some random model request";
//		MLOG(3)<<"Config::modelerHostUrl : "<<Config::modelerHostUrl;
//		std::string url = StringUtil::toStr("__MODELER_URL__") + StringUtil::toStr("/model");
//		url = StringUtil::replaceString(url, StringUtil::toStr("__MODELER_URL__"),
//				Config::modelerHostUrl);
//		LOG(INFO)<<"sending model request, url "<<url);
//		std::string modelResponse = HttpUtil::sendPostRequest(url,
//				modelRequest->toJson());
//
//		return modelResponse;
//	}
//};
//void readProperties() {
//
//	gicapods::ConfigService* configService= std::make_shared<gicapods::ConfigService>("loadtester-modeler.properties");
//	LOG(INFO)<<"configs were loaded..";
//}
//void threadPoolStarter() {
//	ModelerTask::startThreadPool();
//}
//void startModeler(int argc, char** argv, ModelerPtr modeler) {
//	MLOG(3)<<"about to run the modeler.";
//	modeler->run(argc, argv);
//}
//
//int Main(int argc, char** argv) {
//	GalaxieSetup::setup();
////	gicapods::ConfigService* configService= std::make_shared<gicapods::ConfigService>("loadtester-modeler.properties");
//       auto cassandraDriver = std::make_shared<CassandraDriver>(configService);

//
//	readProperties();
//
//	std::thread startThreadPool(threadPoolStarter);
//	startThreadPool.detach();
//
//	ModelerPtr modeler(new Modeler());
//	std::thread startModelerThread(startModeler, argc, argv, modeler);
//	startModelerThread.detach();
//
//	LOG(INFO)<<"An instance of modeler is starting..");
//	LOG(INFO)<<"press 's' to send a model request to modeler");
//	LOG(INFO)<<"press 'q' to exit");
//	ModelerTestsHelper* modelerTester = new ModelerTestsHelper();
//	while (1) { /* skip leading whitespace */
//		int c = getchar();
//		if (c == 's') {
//			LOG(INFO)<<"press 's' to send a bid request to modeler");
//			LOG(INFO)<<"press 'q' to exit");
//			modelerTester->sendModelRequest();
//		} else if (c == 'q') {
//			break;
//		}
//	}
//	delete modelerTester;
//}
///*
// definition of all static member variables
// */
//STATIC_DEFINITION_MACRO_THIS_SHOULD_STAY
//;
///* end of definitions */
