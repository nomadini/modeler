#include "GUtil.h"


#include "CassandraDriverInterface.h"

#include "ConfigService.h"
#include "ModelerTask.h"
#include "ModelerPersistenceService.h"
#include "ModelRequest.h"
#include "ModelerTestsHelper.h"
#include "TestsCommon.h"
#include "DeviceHistory.h"
#include "Offer.h"
#include "ModelerFunctionalTestsHelper.h"
#include "ModelUtil.h"
#include "ModelerFunctionalTests_creatingOneModel.h"


#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "ModelDtoParser.h"
#include "DateTimeUtil.h"
#include "MySqlOfferService.h"
#include "ModelRequestTestHelper.h"
#include "CassandraDriverInterface.h"


ModelerFunctionalTests_creatingOneModel::ModelerFunctionalTests_creatingOneModel() {

}

void ModelerFunctionalTests_creatingOneModel::SetUp() {
        TestBase::SetUp();
}

void ModelerFunctionalTests_creatingOneModel::TearDown() {
        // code here will be called just after the test completes
        // ok to through exceptions from here if need be
        TestBase::TearDown();
}

ModelerFunctionalTests_creatingOneModel::~ModelerFunctionalTests_creatingOneModel() {
        // cleanup any pending stuff, but no exceptions allowed
}

std::shared_ptr<Offer> ModelerFunctionalTests_creatingOneModel::givenDataOfferDataInMySql() {
        std::shared_ptr<Offer> offer = ModelerTestsHelper::getRandomOfferWithRandomPixel (2);
        ModelerFunctionalTestsHelper::mySqlOfferService()->persistOffer (offer);

        std::shared_ptr<Offer> offer2 = ModelerFunctionalTestsHelper::mySqlOfferService()->readOfferByName (offer->name);

        ModelerFunctionalTestsHelper::offerCacheService()->reloadCaches();

        std::shared_ptr<Offer> offerFromCache = ModelerFunctionalTestsHelper::offerCacheService()->findOfferById (offer2->id);

        assertAndThrow(offerFromCache.get () != NULL);
        assertAndThrow(offerFromCache->id == offer2->id);

        return offer2;
}

std::vector<std::shared_ptr<PixelDeviceHistory> > ModelerFunctionalTests_creatingOneModel::insertPixelDeviceHistory(std::shared_ptr<Offer> offer,
                                                                                                                    PixelDeviceHistoryCassandraService* pixelDeviceHistoryCassandraService) {

        std::vector<std::shared_ptr<PixelDeviceHistory> > pixelDeviceHistories;

        if (offer->pixelIds.empty ()) {
                throwEx("there is not pixelIds attached to this offer : " + offer->name);
        }

        for(int pixelId :  offer->pixelIds) {

                std::shared_ptr<PixelDeviceHistory> pixelDeviceHistory = ModelerTestsHelper::createListOfPixelDeviceHistory (2);
                //TODO : get the pixel unique key for the next line
                pixelDeviceHistory->pixelUniqueKey = StringUtil::toStr(pixelId);
                MLOG(3)<<"inserting this pixelDeviceHistory : " << pixelDeviceHistory->toJson();
                // pixelDeviceHistoryCassandraService->writeBatchData (
                //         pixelDeviceHistory); //TODO : fix the argument here
                pixelDeviceHistories.push_back (pixelDeviceHistory);

        }

        return pixelDeviceHistories;
}

void ModelerFunctionalTests_creatingOneModel::givenFeatureAndDeviceHistoryInCassandra(std::shared_ptr<Offer> offer,
                                                                                      PixelDeviceHistoryCassandraService* pixelDeviceHistoryCassandraService,
                                                                                      DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService) {

        std::vector<std::shared_ptr<PixelDeviceHistory> > pixelDeviceHistories = insertPixelDeviceHistory (offer, pixelDeviceHistoryCassandraService);
        if (pixelDeviceHistories.empty ()) {
                throwEx("no deviceHistories were added to cassandra");
        }
        std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory =
                DeviceFeatureHistoryTestHelper::createNewDeviceFeatureHistory ();

        BOOST_FOREACH(std::shared_ptr<PixelDeviceHistory>
                      pixelDeviceHistory, pixelDeviceHistories) {
                //BOOST_FOREACH_MAP

                for(map_t::value_type & entry :
                    *pixelDeviceHistory->devicesHittingPixel) {
                        deviceFeatureHistory->device = entry.second;
                        // deviceFeatureHistoryCassandraService->upsertDeviceFeatureHistory (
                        //         bh, true); //use the batch method for CassandraService

                }
        }


}

std::vector<std::shared_ptr<ModelRequest> > ModelerFunctionalTests_creatingOneModel::whenRequestHitsTheModelerService(
        std::string jsonRequest) {

        MLOG(3) << "sending some random pixel model request";
        std::string url = StringUtil::toStr ("__MODELER_URL__") + StringUtil::toStr ("/model");
        auto configService = std::make_shared<gicapods::ConfigService> ("common-test.properties", "common-test.properties");
        url = StringUtil::replaceString (url, StringUtil::toStr ("__MODELER_URL__"),
                                         configService->get ("modelerHostUrl"));
        LOG(INFO) << "sending modeling request, url " << url;
        std::string modelResponse = HttpUtil::sendPostRequest (url, jsonRequest);

        LOG(INFO) << "response from modeler : " << modelResponse;

        std::shared_ptr<ModelRequest> modelRequest = ModelDtoParser::parseModelDtoOutOfJson (modelResponse);

        std::vector<std::shared_ptr<ModelRequest> > models;
        models.push_back (modelRequest);
        return models;
}

void ModelerFunctionalTests_creatingOneModel::thenModelCreatedIsAsExpected(std::shared_ptr<ModelRequest> actual,
                                                                           std::shared_ptr<ModelRequest> expected,
                                                                           std::string modelType) {

        MLOG(3) << "this is the model created : " << actual->toJson ();
        MLOG(3) << "this is the expected model to be created "<< expected->toJson();
        EXPECT_THAT(actual->modelType, testing::Eq (modelType));
        EXPECT_THAT(actual->algorithmToModelBasedOn, testing::Eq ("alpha"));
        EXPECT_THAT(actual->segmentSeedName, testing::Eq (expected->segmentSeedName));
}

void ModelerFunctionalTests_creatingOneModel::thenModelExistsInDb(std::shared_ptr<ModelRequest> expected) {

        std::shared_ptr<ModelRequest> modelFromDb = ModelerFunctionalTestsHelper::mySqlModelService()->readModelByNameAndAdvertiserId (expected->name,
                                                                                                                                       expected->advertiserId);
        MLOG(3) << "this is the model read from db : " << modelFromDb->toJson ();
        MLOG(3) << "this is the expected model "<< expected->toJson();
        EXPECT_THAT(modelFromDb->name, testing::Eq (expected->name));
        EXPECT_THAT(modelFromDb->modelType, testing::Eq (expected->modelType));
        EXPECT_THAT(modelFromDb->algorithmToModelBasedOn, testing::Eq (expected->algorithmToModelBasedOn));
        EXPECT_THAT(modelFromDb->segmentSeedName, testing::Eq (expected->segmentSeedName));

}

std::shared_ptr<ModelRequest> ModelerFunctionalTests_creatingOneModel::givenAPixelModelRequest(std::shared_ptr<Offer> offer) {


        auto pixelModelRequest = ModelRequestTestHelper::createPixelModelRequest ();
        pixelModelRequest->positiveOfferIds.clear ();
        pixelModelRequest->negativeOfferIds.clear ();
        pixelModelRequest->positiveOfferIds.push_back ((offer->id));
        pixelModelRequest->negativeOfferIds.push_back (offer->id);
        return pixelModelRequest;
}

void ModelerFunctionalTests_creatingOneModel::thenTestWaitsUntilModelingIsDone() {

        ModelerTestsHelper::readModelingDoneNotification ();
}


TEST_F(ModelerFunctionalTests_creatingOneModel, sendSeedModelRequestAndVerifyModelWasBuilt) {
        MLOG(3) << "starting sendSeedModelRequestAndVerifyModelWasBuilt test";

        auto seedModelRequest = givenASeedModelRequest ();

        auto eventListener = ModelerFunctionalTests_creatingOneModel::listenForEvents (
                std::dynamic_pointer_cast<ModelRequest> (seedModelRequest));

        givenDeviceHistoryInCassandraForSeedWebsites (
                seedModelRequest->seedWebsites, deviceFeatureHistoryCassandraService,featureDeviceHistoryCassandraService );

        std::vector<std::shared_ptr<ModelRequest> > models = ModelerFunctionalTests_creatingOneModel::whenRequestHitsTheModelerService (
                seedModelRequest->toJson ());

        std::shared_ptr<ModelRequest> actual = models.at (0);
        std::shared_ptr<ModelRequest> expected = std::dynamic_pointer_cast<ModelRequest> (seedModelRequest);
        ModelerFunctionalTests_creatingOneModel::thenModelCreatedIsAsExpected (actual, expected,
                                                                               StringUtil::toStr ("seedModel"));

        ModelerFunctionalTests_creatingOneModel::thenModelExistsInDb (expected);
        ModelerFunctionalTests_creatingOneModel::thenTestWaitsUntilModelingIsDone ();

        MLOG(3) << "end of test!";

}


TEST_F(ModelerFunctionalTests_creatingOneModel, sendPixelModelRequestAndVerifyModelWasBuilt) {

        std::shared_ptr<Offer> offer = ModelerFunctionalTests_creatingOneModel::givenDataOfferDataInMySql ();
        auto pixelModelRequest = ModelerFunctionalTests_creatingOneModel::givenAPixelModelRequest (offer);
        auto eventListener = ModelerFunctionalTests_creatingOneModel::listenForEvents (
                std::dynamic_pointer_cast<ModelRequest> (pixelModelRequest));
        ModelerFunctionalTests_creatingOneModel::givenFeatureAndDeviceHistoryInCassandra (offer,
                                                                                          pixelDeviceHistoryCassandraService,
                                                                                          deviceFeatureHistoryCassandraService);

        std::vector<std::shared_ptr<ModelRequest> > models = ModelerFunctionalTests_creatingOneModel::whenRequestHitsTheModelerService (
                pixelModelRequest->toJson ());

        std::shared_ptr<ModelRequest> actual = models.at (0);
        std::shared_ptr<ModelRequest> expected = std::dynamic_pointer_cast<ModelRequest> (pixelModelRequest);
        ModelerFunctionalTests_creatingOneModel::thenModelCreatedIsAsExpected (actual, expected,
                                                                               StringUtil::toStr ("pixelModel"));

        ModelerFunctionalTests_creatingOneModel::thenModelExistsInDb (expected);
        ModelerFunctionalTests_creatingOneModel::thenTestWaitsUntilModelingIsDone ();
}
