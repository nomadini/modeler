#include "ARFFFileCreatorTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlDriver.h"
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "OfferTestHelper.h"

#include "ARFFfileCreator.h"
#include "FeatureFetcherService.h"
#include "NegativeFeatureFetcher.h"
#include "PositiveFeatureFetcher.h"
#include "ModelDataProvider.h"
#include "ModelerFunctionalTestsHelper.h"
#include "EntityToModuleStateStats.h"

ARFFFileCreatorTests::ARFFFileCreatorTests() {

}

void ARFFFileCreatorTests::SetUp() {
        TestBase::SetUp();

        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();

        std::shared_ptr<FeatureFetcherService<DeviceFeatureHistory> > featureFetcherService =
                std::make_shared<FeatureFetcherService>(
                        pixelDeviceHistoryCassandraService,
                        ModelerFunctionalTestsHelper::offerCacheService(),
                        deviceFeatureHistoryCassandraService,
                        featureDeviceHistoryCassandraService);

        featureFetcherService->entityToModuleStateStats = entityToModuleStateStats;
        auto negativeFeatureFetcher = std::make_shared<NegativeFeatureFetcher>();
        negativeFeatureFetcher->entityToModuleStateStats = entityToModuleStateStats;
        auto positiveFeatureFetcher = std::make_shared<PositiveFeatureFetcher>(featureFetcherService);
        positiveFeatureFetcher->entityToModuleStateStats = entityToModuleStateStats;

        auto modelDataProvider = std::make_shared<ModelDataProvider>(negativeFeatureFetcher, positiveFeatureFetcher);
        modelDataProvider->entityToModuleStateStats = entityToModuleStateStats;
}

void ARFFFileCreatorTests::TearDown() {
        TestBase::TearDown();
}


TEST_F(ARFFFileCreatorTests, createArffFile) {
        std::vector<std::shared_ptr<ARFFFRecord> > records;
        std::set<std::string> listOfClasses;
        listOfClasses.insert("c1");
        listOfClasses.insert("c2");

        std::set<int> orderedListOfFeatures;

        for (int i=0; i < 3; i++) {
                std::unordered_map<int, int> featureToValueMap;

                //orderedListOfFeatures
                featureToValueMap.insert(std::make_pair(1, 2));
                orderedListOfFeatures.insert(1);
                featureToValueMap.insert(std::make_pair(2, 4));
                orderedListOfFeatures.insert(2);
                featureToValueMap.insert(std::make_pair(3, 6));
                orderedListOfFeatures.insert(3);

                std::shared_ptr<ARFFFRecord> record = std::make_shared<ARFFFRecord>();
                record->featureToValueMap = featureToValueMap;
                record->className = "c1";
                records.push_back(record);
        }

        std::shared_ptr<ARFFFileCreator> aRFFFileCreator = std::make_shared<ARFFFileCreator>(modelDataProvider);
        aRFFFileCreator->createFile("/tmp/mySample.txt", records, orderedListOfFeatures, listOfClasses);
}

TEST_F(ARFFFileCreatorTests, createARFFFileFromCassandraData) {

        auto seedModelRequest = givenASeedModelRequest ();

        givenDeviceHistoryInCassandraForSeedWebsites (
                seedModelRequest->seedWebsites, deviceFeatureHistoryCassandraService,featureDeviceHistoryCassandraService );

        std::vector<std::shared_ptr<DeviceFeatureHistory> > deviceFeatureHistories= modelDataProvider->provideDeviceHistoriesBasedOnSeedModel(seedModelRequest);

        std::set<int> orderedListOfFeaturesOutput;
        std::set<std::string> orderedListOfClassesOutput;


        std::shared_ptr<ARFFFileCreator> aRFFFileCreator= std::make_shared<ARFFFileCreator>(modelDataProvider);

        std::vector<std::shared_ptr<ARFFFRecord> > records = aRFFFileCreator->createRecordsFromDeviceFeatureHistories(
                deviceFeatureHistories,
                orderedListOfFeaturesOutput,
                orderedListOfClassesOutput);

        aRFFFileCreator->createFile("/tmp/mySample.txt", records, orderedListOfFeaturesOutput, orderedListOfClassesOutput);
}
