#include "CreatingModelFromSubmittedRequestInDB.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlDriver.h"
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "OfferTestHelper.h"

#include "CassandraDriverInterface.h"
#include "HttpUtilService.h"
#include "HttpUtilService.h"
#include "MySqlModelService.h"
CreatingModelFromSubmittedRequestInDB::CreatingModelFromSubmittedRequestInDB() {

}

void CreatingModelFromSubmittedRequestInDB::SetUp() {
        // code here will execute just before the test ensues
        TestBase::SetUp();
}

void CreatingModelFromSubmittedRequestInDB::TearDown() {
        // code here will be called just after the test completes
        // ok to through exceptions from here if need be

        MLOG(3) << "going to sleep to  let the threads break out of their loops....";
        gicapods::Util::sleepViaBoost (_L_, 5);
}

std::shared_ptr<ModelRequest> CreatingModelFromSubmittedRequestInDB::givenAModelRequestWithPositiveOffers(std::shared_ptr<Offer> offer) {
        std::shared_ptr<ModelRequest> model (new ModelRequest ());
        model->segmentSeedName = StringUtil::toStr ("cnn-visitor-today").append (StringUtil::random_string (4));

        model->dateOfRequest = DateTimeUtil::getNowPlusSecondsInMySqlFormat (-10800);
        model->featureRecencyInSecond = 10000;
        model->advertiserId = RandomUtil::sudoRandomNumber (1000);
        model->name = StringUtil::toStr ("svmSgdShogun").append (StringUtil::random_string (4));
        model->pixelHitRecencyInSecond = 10000;
        model->maxNumberOfDeviceHistoryPerFeature = 100;
        model->modelType = "pixelModel";

        model->positiveOfferIds = OfferTestHelper::getRandomOfferIds (10);
        model->negativeOfferIds = OfferTestHelper::getRandomOfferIds (10);//have a test case that fixes
        model->numberOfNegativeDevicesToBeUsed = 100;
        model->numberOfPositiveDevicesToBeUsed = 100;
        model->numberOfTopFeatures = 10;
        model->cutOffScore = 2;

        model->featureRecencyInSecond = 3600;
        model->pixelHitRecencyInSecond = 1000;


        model->algorithmToModelBasedOn = StringUtil::toStr ("alpha");


        model->positiveOfferIds.clear ();
        model->negativeOfferIds.clear ();
        model->positiveOfferIds.push_back ((offer->id));
        model->negativeOfferIds.push_back (offer->id);

        model->validate ();
        return model;
}

std::shared_ptr<ModelRequest> CreatingModelFromSubmittedRequestInDB::givenAModelRequestWithSeedWebsites() {

        std::shared_ptr<ModelRequest> model (new ModelRequest ());
        model->segmentSeedName = StringUtil::toStr ("cnn-visitor-today").append (StringUtil::random_string (4));
        model->dateOfRequest = DateTimeUtil::getNowPlusSecondsInMySqlFormat (-10800);//3 hours ago
        model->featureRecencyInSecond = 10000;
        model->advertiserId = RandomUtil::sudoRandomNumber (1000);
        model->name = StringUtil::toStr ("svmSgdShogun").append (StringUtil::random_string (4));
        model->pixelHitRecencyInSecond = 10000;
        model->maxNumberOfDeviceHistoryPerFeature = 100;
        model->modelType = "seedModel";


        model->seedWebsites.insert (StringUtil::toStr ("www.cnn.com"));
        model->numberOfNegativeDevicesToBeUsed = 100;
        model->numberOfPositiveDevicesToBeUsed = 100;
        model->maxNumberOfDeviceHistoryPerFeature = 100;
        model->numberOfTopFeatures = 10;
        model->cutOffScore = 2;
        model->maxNumberOfNegativeFeaturesToPick = 100;
        model->featureRecencyInSecond = 3600;

        model->algorithmToModelBasedOn = StringUtil::toStr ("alpha");

        model->validate ();
        return model;
}

void CreatingModelFromSubmittedRequestInDB::givenTheRequestIsSubmittedInMySql(std::shared_ptr<ModelRequest> modelPersistentDto) {

        mySqlModelService->insert (modelPersistentDto);
}

void CreatingModelFromSubmittedRequestInDB::whenAsynchronousModelerThreadIsRun() {
        auto httpUtilService = std::make_shared<HttpUtilService>();

        auto offerCacheService = std::make_shared<CacheService<Offer> > (
                ModelerFunctionalTestsHelper::mySqlOfferService(),
                httpUtilService,
                "fixmeLater",
                entityToModuleStateStats,
                "testApp");

        auto modelerPersistenceService = std::make_shared<ModelerPersistenceService> ();
        modelerPersistenceService->mySqlModelService = mySqlModelService;
        // modelerPersistenceService->mySqlModelResultService = mySqlModelResultService;

        modelerPersistenceService->entityToModuleStateStats = entityToModuleStateStats;


        Modeler::runModellingOnRequestOlderThanNHours (2, mySqlModelService);
        MLOG(3) << "realTimeUpdater , waiting for async modeler to build the model, 10 seconds";
        gicapods::Util::sleepViaBoost (_L_, 10);
}

void CreatingModelFromSubmittedRequestInDB::thenRequestIsPickedUpAndModelIsBuiltAsExpected(
        std::shared_ptr<ModelRequest> expected, std::string modelType) {

        std::shared_ptr<ModelRequest> modelFromDb = mySqlModelService->readModelByNameAndAdvertiserId (
                expected->name, expected->advertiserId);
        MLOG(3) << "this is the model read from db : " << modelFromDb->toJson ();

        EXPECT_THAT(modelFromDb->name, testing::Eq (expected->name));
        EXPECT_THAT(modelFromDb->modelType, testing::Eq (expected->modelType));
        EXPECT_THAT(modelFromDb->algorithmToModelBasedOn, testing::Eq (expected->algorithmToModelBasedOn));
        EXPECT_THAT(modelFromDb->segmentSeedName, testing::Eq (expected->segmentSeedName));
        EXPECT_THAT(modelFromDb->featureScoreMap.size (), testing::Gt (0));


        if (modelType.compare ("seedModel") == 0) {

                EXPECT_THAT(modelFromDb->seedWebsites.size (), testing::Eq (expected->seedWebsites.size ()));
                EXPECT_THAT(modelFromDb->maxNumberOfDeviceHistoryPerFeature,
                            testing::Eq (expected->maxNumberOfDeviceHistoryPerFeature));
                EXPECT_THAT(modelFromDb->maxNumberOfNegativeFeaturesToPick,
                            testing::Eq (expected->maxNumberOfNegativeFeaturesToPick));

        } else if (modelType.compare ("pixelModel") == 0) {

                EXPECT_THAT(modelFromDb->negativeOfferIds.size (), testing::Eq (expected->negativeOfferIds.size ()));
                EXPECT_THAT(modelFromDb->positiveOfferIds.size (), testing::Eq (expected->positiveOfferIds.size ()));
                EXPECT_THAT(modelFromDb->pixelHitRecencyInSecond, testing::Eq (expected->pixelHitRecencyInSecond));
        } else {
                throwEx("unknow model type");
        }

}


TEST_F(CreatingModelFromSubmittedRequestInDB, buildModelFromSubmittedSeedModelRequest) {
        MLOG(3) << "starting buildModelFromSubmittedModelRequests test";

        std::shared_ptr<ModelRequest> modelPersistentDto = givenAModelRequestWithSeedWebsites ();

        givenDeviceHistoryInCassandraForSeedWebsites (modelPersistentDto->seedWebsites,
                                                      deviceFeatureHistoryCassandraService,
                                                      featureDeviceHistoryCassandraService );

        givenTheRequestIsSubmittedInMySql (modelPersistentDto);

        ModelerFunctionalTests_creatingOneModel::thenModelExistsInDb (
                std::dynamic_pointer_cast<ModelRequest> (modelPersistentDto));

        CreatingModelFromSubmittedRequestInDB::whenAsynchronousModelerThreadIsRun ();

        ModelerFunctionalTests_creatingOneModel::thenTestWaitsUntilModelingIsDone ();

        thenRequestIsPickedUpAndModelIsBuiltAsExpected (modelPersistentDto,"seedModel");


        MLOG(3) << "end of buildModelFromSubmittedModelRequests test!";

}

TEST_F(CreatingModelFromSubmittedRequestInDB, buildModelFromSubmittedPixelModelRequest) {

        MLOG(3) << "starting buildModelFromSubmittedModelRequests test";


        std::shared_ptr<Offer> offer = ModelerFunctionalTests_creatingOneModel::givenDataOfferDataInMySql ();
        //auto eventListener = listenForEvents(std::dynamic_pointer_cast<ModelRequest>(pixelModelRequest));
        ModelerFunctionalTests_creatingOneModel::givenFeatureAndDeviceHistoryInCassandra (offer, pixelDeviceHistoryCassandraService,
                                                                                          deviceFeatureHistoryCassandraService);


        std::shared_ptr<ModelRequest> modelPersistentDto = CreatingModelFromSubmittedRequestInDB::givenAModelRequestWithPositiveOffers (
                offer);


        givenTheRequestIsSubmittedInMySql (modelPersistentDto);

        ModelerFunctionalTests_creatingOneModel::thenModelExistsInDb (
                std::dynamic_pointer_cast<ModelRequest> (modelPersistentDto));

        whenAsynchronousModelerThreadIsRun ();

        ModelerFunctionalTests_creatingOneModel::thenTestWaitsUntilModelingIsDone ();

        thenRequestIsPickedUpAndModelIsBuiltAsExpected (modelPersistentDto,"pixelModel");


        MLOG(3) << "end of buildModelFromSubmittedModelRequests test!";

}
