/*
 * FeatureBasedModeler.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef MODELER_TESTS_HELPER_H_
#define MODELER_TESTS_HELPER_H_


#include "ModelRequest.h"
#include "PixelDeviceHistory.h"

#include "Offer.h"
class DeviceFeatureHistory;
#include "Poco/NotificationQueue.h"
#include <string>
#include <memory>
#include <vector>

class ModelerTestsHelper {

public:


    static std::shared_ptr<Offer> getRandomOfferWithRandomPixel(int size);

    static std::shared_ptr<PixelDeviceHistory> createListOfPixelDeviceHistory(int size);
    static std::shared_ptr<DeviceFeatureHistory> createDeviceFeatureHistory(std::string prefix, int size);

    static std::vector<std::shared_ptr<DeviceFeatureHistory>> createMockDeviceFeatureHistories(std::string prefix, int size) ;
};


#endif /* MODELER_TESTS_HELPER_H_ */
