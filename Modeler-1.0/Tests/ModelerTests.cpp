/*
 * Modeler.cpp
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#include "GUtil.h"


#include "CassandraDriverInterface.h"

#include "ConfigService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "ModelerFunctionalTestsHelper.h"
#include "ModelUtil.h"

#include "GUtil.h"
#include "StringUtil.h"
#include "GalaxieSetup.h"
#include <string>
#include <memory>
#include "MySqlDriver.h"
#include "MySqlModelService.h"
#include <thread>
int runGoogleTests(int argc, char **argv) {
        // The following line must be executed to initialize Google Mock
        // (and Google Test) before running the tests.
        ::testing::InitGoogleMock (&argc, argv);
        ::testing::FLAGS_gmock_verbose = "info";

        return RUN_ALL_TESTS ();
}

void readProperties() {


}

void runModeler() {

        GalaxieSetup::setup ();

        ModelerFunctionalTestsHelper::modeler()->run (ModelerFunctionalTestsHelper::argc, ModelerFunctionalTestsHelper::argv);
}

void givenModelerRunning() {

        std::thread everyMinuteThread (runModeler);
        gicapods::Util::sleepViaBoost (_L_, 4);//wait for modeler to start up!!
        everyMinuteThread.detach ();
}

int main(int argc, char* argv[]) {


        SignalHandler::installHandlers ();
        google::InstallFailureSignalHandler ();
        google::InitGoogleLogging(*argv);
        FLAGS_logtostderr = true;
//    FLAGS_log_dir = "/some/log/directory";
//    FLAGS_stderrthreshold = 0;
        FLAGS_colorlogtostderr = true;
//    FLAGS_minloglevel = 0;
        FLAGS_v = 10;//print out all from 0 to 10 levels
//    FLAGS_v = 1;//print out all from 0 to 10 levels
        FLAGS_alsologtostderr = true;
        LogLevelManager::enableAll ();
        if (argc < 2) {
                ModelerFunctionalTestsHelper::init (argc, argv);
        } else{
                std::string gtestArgument = argv[1];
                if (StringUtil::contains(gtestArgument, "--gtest") ) {
                        char* argv1[] = {"./ModelerTests.out"};
                        ModelerFunctionalTestsHelper::init (1, argv1);
                }
        }


        readProperties ();
        auto configService = std::make_shared<gicapods::ConfigService> (
                "modeler-test.properties", "common-test.properties");
        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();
        auto cassandraDriver = std::make_shared<CassandraDriver>(configService, entityToModuleStateStats);

        givenModelerRunning ();
        MLOG(3) <<"arguments are argc : "<< argc << " , argv [0]: "<< argv[0] <<"  , argv [1]: "<< argv[1];
        runGoogleTests (argc, argv);



        cassandraDriver->closeSessionAndCluster ();
}
