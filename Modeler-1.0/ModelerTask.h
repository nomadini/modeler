/*
 * ModelerTask.h
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_SVMSGDWORKER_H_
#define GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_SVMSGDWORKER_H_

class ModelerPersistenceService;
class ModelerContext;

#include "Poco/NotificationQueue.h"
#include "FeatureBasedModeler.h"
#include "ModelerModule.h"

class EntityToModuleStateStats;
class ModelDataCleaner;

class ModelerTask : public Poco::Runnable {
public:

std::shared_ptr<ModelerContext> modelerContext;

EntityToModuleStateStats* entityToModuleStateStats;
NegativeFeatureFetcher* negativeFeatureFetcher;
PositiveFeatureFetcher* positiveFeatureFetcher;
ModelDataCleaner* modelDataCleaner;
FeatureBasedModeler* featureBasedModeler;
ModelerPersistenceService* modelerPersistenceService;
std::vector<ModelerModule*> modules;
ModelerTask();

void updateModelAndResult(std::shared_ptr<ModelerContext> context);
void initialProcessing();
void initModules();

virtual void run();

bool isEliggibleToRun();

virtual ~ModelerTask();
private:

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_SVMSGDWORKER_H_ */
