

#include "GUtil.h"
#include "StringUtil.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "FileUtil.h"
#include "ModelRecord.h"

void ModelRecord::add(int index, double feature) {
								auto ptr = indexFeatureCombos.find(index);
								if (ptr != indexFeatureCombos.end()) {
																ptr->second = feature;
																return;
								}
								indexFeatureCombos.insert(std::pair<int, double>(index, feature));
}

std::vector<std::shared_ptr<ModelRecord> > ModelRecord::convertToModelRecords(
								std::vector<std::string> records, int label) {
								std::vector<std::shared_ptr<ModelRecord> > positiveModelRecords;
								for (const auto& record : records) {

																std::shared_ptr<ModelRecord> modelRecord = std::make_shared<ModelRecord> ();
																modelRecord->label = StringUtil::toStr(label);
																modelRecord->record = StringUtil::toStr(record);

																positiveModelRecords.push_back(modelRecord);
								}
								MLOG(3)<<"positiveModelRecords.size() : "<<positiveModelRecords.size();

								return positiveModelRecords;
}

void ModelRecord::createLibSvmFile(std::vector<std::shared_ptr<ModelRecord> > allModelRecords,
																																			std::string featuresFileName) {
								FileUtil::deleteFile(featuresFileName);
								bool first = true;
								FileUtil::createFileIfNotExist(featuresFileName);
								for (const auto& record : allModelRecords) {
																std::string recordStr;

																if (record->label.compare("-1") == 0) {
																								recordStr.append(StringUtil::toStr("0"));
																} else {
																								recordStr.append(StringUtil::toStr("1"));
																}
																recordStr.append(StringUtil::toStr(" "));
																recordStr.append(record->record);

																FileUtil::appendALineToFile(featuresFileName, recordStr);
								}
}

void ModelRecord::createFeaturesFile(std::vector<std::shared_ptr<ModelRecord> > allModelRecords,
																																					std::string featuresFileName) {
								FileUtil::deleteFile(featuresFileName);
								FileUtil::createFileIfNotExist(featuresFileName);
								for (const auto& record : allModelRecords) {
																FileUtil::appendALineToFile(featuresFileName, record->record);
								}
								MLOG(3)<<"count of lines in feature file :" << FileUtil::getCountOfLinesInFile(featuresFileName);
}

void ModelRecord::createLabelsFile(std::vector<std::shared_ptr<ModelRecord> > allModelRecords,
																																			std::string labelsFileName) {
								FileUtil::deleteFile(labelsFileName);
								FileUtil::createFileIfNotExist(labelsFileName);
								for (const auto& record : allModelRecords) {
																FileUtil::appendALineToFile(labelsFileName, record->label);

								}
								MLOG(3)<<"count of lines in label file : " << FileUtil::getCountOfLinesInFile(labelsFileName);
}
