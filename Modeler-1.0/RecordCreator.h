/*
 * RecordCreator.h
 *
 *  Created on: Oct 23, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_RECORDCREATOR_H_
#define GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_RECORDCREATOR_H_


#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

class DeviceFeatureHistory;

class RecordCreator {
public :

	/**
	 * this creates a record in the format csv to be used by FeatureBasedModeler
	 */
	static std::string createOneRecord(std::shared_ptr<DeviceFeatureHistory> browserHistory,
			std::unordered_map<std::string, int>& featureIndexMap);

	static std::vector<std::string> createRecordsWithNoLabelForModel(
				std::vector<std::shared_ptr<DeviceFeatureHistory>> browserHistories,
				std::unordered_map<std::string, int>& featureIndexMap);

	/**
	 * creates a record for BudgetedSvm models
	 */
	static std::string createRecordForModel(
			std::unordered_map<std::string, int>& featureIndexMap,
			std::set<std::string> allFeatures, int label,
			int maxDimension = -1);
};



#endif /* GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_RECORDCREATOR_H_ */
