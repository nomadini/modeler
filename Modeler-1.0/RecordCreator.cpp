

#include "GUtil.h"



#include "IntWrapper.h"
#include "MySqlDriver.h"
#include "DeviceFeatureHistory.h"
#include "SgdSolver.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "RecordCreator.h"

#include "FileUtil.h"
#include "ModelRecord.h"
#include "GUtil.h"
#include "StringUtil.h"

#include "RecordCreator.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>


std::string RecordCreator::createOneRecord(std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory,
                                           std::unordered_map<std::string, int>& featureIndexMap) {

        std::string record;
        for (auto feature : *deviceFeatureHistory->getFeatures()) {

                auto uniqueIdPtr = featureIndexMap.find(feature->feature->getName());
                if (uniqueIdPtr == featureIndexMap.end()) {
                        //cout("feature not found");
                } else {
                        // found
                        int index = uniqueIdPtr->second;
                        //std::cout <<"feature : " << uniqueIdPtr->first << " has index  " << uniqueIdPtr->second << std::endl;

                        record.append(StringUtil::toStr(index));
                        record.append(":");
                        record.append("10 ");

                }
        }

        //	traceme(record);
        return record;
}

std::vector<std::string> RecordCreator::createRecordsWithNoLabelForModel(
        std::vector<std::shared_ptr<DeviceFeatureHistory> > browserHistories,
        std::unordered_map<std::string, int>& featureIndexMap) {

        std::vector<std::string> allRecords;

        for (std::vector<std::shared_ptr<DeviceFeatureHistory> >::iterator it =
                     browserHistories.begin(); it != browserHistories.end(); ++it) {
                std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory = *it;
                allRecords.push_back(createOneRecord(deviceFeatureHistory, featureIndexMap));
        }
        return allRecords;
}

/**
 * creates a record for BudgetedSvm models
 */
std::string RecordCreator::createRecordForModel(
        std::unordered_map<std::string, int>& featureIndexMap,
        std::set<std::string> allFeatures, int label,
        int maxDimension) {
        std::string record;
        assertAndThrow(allFeatures.size() != 0);

        if (label == 1)
                record.append("+1 ");
        else
                record.append("-1 ");
        assert(allFeatures.size() > 0);
        for (std::set<std::string>::iterator it = allFeatures.begin();
             it != allFeatures.end(); ++it) {
                if (featureIndexMap.find(*it) == featureIndexMap.end()) {
                        // not found

                        assert(false);
                } else {
                        // found

                        std::unordered_map<std::string, int>::iterator wanterIter =
                                featureIndexMap.find(*it);

                        std::string feature = wanterIter->first;

                        int index = wanterIter->second;

                        if (maxDimension != -1) {
                                //this means maxDimension has been
                                //passed and we are creating a record for prediciton, and
                                //not model building

                                if (index > maxDimension) {
                                        continue; //we should ignore this index
                                }

                        }
                        ////cout("feature : " + StringUtil::toStr(feature) + " , index : "+ StringUtil::toStr(index));
                        record.append(StringUtil::toStr(index));
                        record.append(":");
                        record.append("1 ");

                }
        }
        return record;
}
