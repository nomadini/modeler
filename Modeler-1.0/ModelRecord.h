/*
 * ModelRecord.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef MODELRECORD_H
#define MODELRECORD_H


#include "FileUtil.h"
/**
 * combines a label and feature string that is in libsvm format
 */
class ModelRecord {
public:

std::string label;
std::string record;  // index1:feature1 index2:feature2 index3:feature3
std::unordered_map<int, double> indexFeatureCombos;

void add(int index, double feature);
static std::vector<std::shared_ptr<ModelRecord> > convertToModelRecords(
								std::vector<std::string> records, int label);

static void createLibSvmFile(std::vector<std::shared_ptr<ModelRecord> > allModelRecords,
																													std::string featuresFileName);

static void createFeaturesFile(std::vector<std::shared_ptr<ModelRecord> > allModelRecords,
																															std::string featuresFileName);
static void createLabelsFile(std::vector<std::shared_ptr<ModelRecord> > allModelRecords,
																													std::string labelsFileName);

};

#endif /* MODELRECORD_H_ */
