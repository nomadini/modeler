



#include "GUtil.h"
#include "FeatureBasedModeler.h"
#include "ModelerPersistenceService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PositiveFeatureFetcher.h"
#include "NegativeFeatureFetcher.h"
#include "MySqlModelService.h"
#include "MySqlModelResultService.h"
#include "DateTimeUtil.h"
#include "ModelerTask.h"
#include "ExceptionUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "Poco/AutoPtr.h"
#include <string>
#include <memory>
#include <boost/exception/all.hpp>
#include "FeatureBasedModeler.h"
#include "ModelerPersistenceService.h"
#include "EntityToModuleStateStats.h"
#include "FeatureFetcherService.h"
#include "PositiveFeatureFetcher.h"
#include "NegativeFeatureFetcher.h"
#include "ModelRequest.h"
#include "ModelerContext.h"
#include "JsonArrayUtil.h"
#include "ModelDataCleaner.h"
#include "ModelerPersistenceService.h"
#include "NegativeFeatureFetcher.h"
#include "PositiveFeatureFetcher.h"

ModelerTask::ModelerTask() {
}

void ModelerTask::initModules() {

        modules.push_back((ModelerModule*)positiveFeatureFetcher);
        modules.push_back((ModelerModule*)negativeFeatureFetcher);
        modules.push_back((ModelerModule*)modelDataCleaner);
        modules.push_back((ModelerModule*)featureBasedModeler);
        modules.push_back((ModelerModule*)modelerPersistenceService);
}

void ModelerTask::run() {

        initialProcessing();

        for (auto module : modules) {

                try {
                        auto startTime = DateTimeUtil::getNowInSecond();
                        LOG(INFO)<<"started processing module "<<module->getName();
                        module->process(modelerContext);
                        auto timeDiffInSeconds = DateTimeUtil::getNowInSecond();
                        LOG(INFO)<<"finished processing module "<<module->getName()
                                 << " in " << (float) (timeDiffInSeconds / 60) <<" minutes";
                } catch (...) {
                        entityToModuleStateStats->
                        addStateModuleForEntity(
                                "exceptionInModeler:UnknownException",
                                "ModelerTask",
                                EntityToModuleStateStats::all,
                                EntityToModuleStateStats::exception);
                        gicapods::Util::showStackTrace();
                        modelerContext->redFlags.push_back("unknown error happening when handling request");
                }

                if (!modelerContext->redFlags.empty()) {
                        LOG(ERROR)<<"aborting because of redflag. "<<JsonArrayUtil::convertListToJson(
                                modelerContext->redFlags);
                        modelerContext->modelRequest->modelResult->processResult = ModelResult::PROCESS_RESULT_FAILED;
                        modelerContext->modelRequest->modelResult->redFlags = modelerContext->redFlags;
                        updateModelAndResult(modelerContext);
                        break;
                }
        }


}

void ModelerTask::initialProcessing() {
        LOG(INFO)<<"svmSgd worker is processing this request : "<< modelerContext->modelRequest->toshortString();
        modelerContext->modelRequest->validate();
        modelerContext->modelRequest->modelResult->processResult = ModelResult::PROCESS_RESULT_STARTED;
        updateModelAndResult(modelerContext);
}

void ModelerTask::updateModelAndResult(
        std::shared_ptr<ModelerContext> context) {
        modelerPersistenceService->mySqlModelService->upsertModel (context->modelRequest);
        context->modelRequest->modelResult->modelRequestId = context->modelRequest->id;
        modelerPersistenceService->mySqlModelResultService->upsertModel (context->modelRequest->modelResult);

}

bool ModelerTask::isEliggibleToRun() {
        if(modelerContext->modelRequest->rebuildModelOnNextRun) {
                //rebuilding model or model variation per user request
                //no matter if the model is created or not
                return true;
        }

        if (modelerContext->modelRequest->modelResult != nullptr &&
            StringUtil::equalsIgnoreCase(modelerContext->modelRequest->modelResult->processResult,
                                         ModelResult::PROCESS_RESULT_CREATED)) {
                return false;
        }

        int diffInSeconds = DateTimeUtil::getNowInSecond() -
                            modelerContext->modelRequest->modelResult->
                            updatedAt.timestamp().epochTime();


        MLOG(3)<<"model diffInSeconds : "
               <<diffInSeconds
               << " , modelResult->updatedAt.timestamp().epochTime() : "
               <<modelerContext->modelRequest->modelResult->
                updatedAt.timestamp().epochTime();

        if (modelerContext->modelRequest->modelResult != nullptr &&
            diffInSeconds < 120) {
                MLOG(3)<<"model was build less than 2 minutes ago, so not eligible to run.diffInSeconds : "
                       <<diffInSeconds
                       << "modelResult->updatedAt.timestamp().epochTime() : "
                       <<modelerContext->modelRequest->modelResult->
                        updatedAt.timestamp().epochTime();
                return false;
        }

        return true;
}

ModelerTask::~ModelerTask() {

}
