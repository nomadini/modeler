#ifndef ModelerModule_H
#define ModelerModule_H


#include "Status.h" //this is needed in every ModelerModule
#include "ModelerContext.h"
#include <memory>
#include <string>
class EntityToModuleStateStats;

class ModelerModule {

private:

public:

ModelerModule();

virtual void process(std::shared_ptr<ModelerContext> context)=0;

virtual std::string getName()=0;

virtual ~ModelerModule();

};

#endif
