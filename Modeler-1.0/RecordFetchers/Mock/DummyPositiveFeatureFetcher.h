/*
 * DummyPositiveFeatureFetcher.h
 *
 *  Created on: Oct 22, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_DummyPositiveFeatureFetcher_H_
#define GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_DummyPositiveFeatureFetcher_H_


#include "PositiveFeatureFetcherInterface.h"
#include <memory>
#include <string>
#include <set>
class DummyPositiveFeatureFetcher : public PositiveFeatureFetcherInterface {
public:

void fetchPositveFeaturesBasedOnPixelModel(std::shared_ptr<ModelerContext> context);

void fetchPositveFeaturesBasedOnSeedModel(std::shared_ptr<ModelerContext> context);

virtual ~DummyPositiveFeatureFetcher();


};

DummyPositiveFeatureFetcher::~DummyPositiveFeatureFetcher() {

}

DummyPositiveFeatureFetcher::fetchPositveFeaturesBasedOnPixelModel(
								std::shared_ptr<ModelerContext> context) {

								for(int i=0; i < 1000; i++) {

																auto pos = ModelerTestsHelper::createDeviceFeatureHistory("positive", 2);
																context->positiveDeviceHistories->push_back(pos);
								}
}


void DummyPositiveFeatureFetcher::fetchPositveFeaturesBasedOnSeedModel(
								std::shared_ptr<ModelerContext> context) {


								for(int i=0; i < 1000; i++) {
																auto pos = ModelerTestsHelper::createDeviceFeatureHistory("negative", 2);
																context->positiveDeviceHistories->push_back(pos);
								}

}


#endif /* GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_DummyPositiveFeatureFetcher_H_ */
