/*
 * FeatureFetcher.h
 *
 *  Created on: Oct 22, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_DummyNegativeFeatureFetcher_H_
#define GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_DummyNegativeFeatureFetcher_H_


#include "NegativeFeatureFetcherInterface.h"
#include "ModelerTestsHelper.h"

class DummyNegativeFeatureFetcher : public NegativeFeatureFetcherInterface {

public:

std::vector<std::shared_ptr<Offer>> getOffers(std::set<int> offerIds);

void fetchNegativeFeaturesBasedOnPixelModel(std::shared_ptr<ModelerContext> context);

void fetchNegativeFeaturesBasedOnSeedModel(std::shared_ptr<ModelerContext> context);

virtual ~DummyNegativeFeatureFetcher();
};

DummyNegativeFeatureFetcher::~DummyNegativeFeatureFetcher() {

}

std::vector<std::shared_ptr<Offer>> DummyNegativeFeatureFetcher::getOffers(std::set<int> offerIds) {
        std::vector<std::shared_ptr<Offer>> offers1;

        std::set<int> pixelIds;

        for(int i=0; i < 10; i++) {
                pixelIds.insert(i);
        }

        for(int i=0; i < 10; i++) {
                std::shared_ptr<Offer> off(new Offer());
                off->pixelIds = pixelIds;
                offers1.insert(off);
        }

        return offers1;
}

std::vector<std::shared_ptr<DeviceFeatureHistory> >
DummyNegativeFeatureFetcher::fetchNegativeFeaturesBasedOnSeedModel(std::shared_ptr<ModelerContext> context) {
        std::vector<std::shared_ptr<DeviceFeatureHistory> > mockNegatives;

        for(int i=0; i < 1000; i++) {
                auto pos = ModelerTestsHelper::createDeviceFeatureHistory("negative",10);
                mockNegatives.push_back(pos);
        }

        return mockNegatives;

}

std::vector<std::shared_ptr<DeviceFeatureHistory> >
DummyNegativeFeatureFetcher::fetchNegativeFeaturesBasedOnPixelModel(std::shared_ptr<ModelerContext> context) {

        std::vector<std::shared_ptr<DeviceFeatureHistory> >  mockNegatives;

        for(int i=0; i < 1000; i++) {
                auto pos = ModelerTestsHelper::createDeviceFeatureHistory("negative",10);
                mockNegatives.push_back(pos);
        }

        return mockNegatives;
}



#endif /* GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_DummyNegativeFeatureFetcher_H_ */
