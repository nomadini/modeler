/*
 * PositiveFeatureFetcher.h
 *
 *  Created on: Oct 22, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_POSITIVEFEATUREFETCHER_H_
#define GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_POSITIVEFEATUREFETCHER_H_

class FeatureFetcherService;
class DeviceFeatureHistoryCassandraService;

#include "FeatureDeviceHistoryCassandraService.h";
#include "DeviceFeatureHistoryCassandraService.h";
#include "ModelerModule.h";
#include "PositiveFeatureFetcherInterface.h";
class ModelerContext;
class EntityToModuleStateStats;


class PositiveFeatureFetcher : public PositiveFeatureFetcherInterface, public ModelerModule {

public:
std::shared_ptr<FeatureFetcherService> featureFetcherService;
DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService;
FeatureDeviceHistoryCassandraService* featureDeviceHistoryCassandraService;

EntityToModuleStateStats* entityToModuleStateStats;
PositiveFeatureFetcher(std::shared_ptr<FeatureFetcherService> featureFetcherServiceArg);

std::string getName();

virtual void process(
        std::shared_ptr<ModelerContext> context);

virtual void fetchPositveFeaturesBasedOnPixelModel(
        std::shared_ptr<ModelerContext> context);

virtual void fetchPositveFeaturesBasedOnSeedModel(
        std::shared_ptr<ModelerContext> context);
virtual ~PositiveFeatureFetcher();
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_POSITIVEFEATUREFETCHER_H_ */
