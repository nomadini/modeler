/*
 * FeatureFetcher.h
 *
 *  Created on: Oct 22, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_NEGATIVEFEATUREFETCHER_INTERFACE_H_
#define GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_NEGATIVEFEATUREFETCHER_INTERFACE_H_

class DeviceFeatureHistory;
class PixelDeviceHistoryCassandraService;
class ModelerContext;
#include <memory>
#include <string>
#include <set>
#include <vector>

class NegativeFeatureFetcherInterface {

public:



virtual void fetchNegativeFeaturesBasedOnSeedModel(std::shared_ptr<ModelerContext> context)=0;

virtual void  fetchNegativeFeaturesBasedOnPixelModel(std::shared_ptr<ModelerContext> context)=0;
virtual ~NegativeFeatureFetcherInterface()   {
}
};


#endif /* GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_NEGATIVEFEATUREFETCHER_INTERFACE_H_ */
