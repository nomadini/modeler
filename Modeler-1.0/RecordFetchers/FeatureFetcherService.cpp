
#include "GUtil.h"
#include "Offer.h"
#include "PixelDeviceHistoryCassandraService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "FeatureFetcherService.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "Pixel.h"
#include "PixelDeviceHistory.h"
#include "OfferPixelMapCacheService.h"
#include "PixelCacheService.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "ModelRequest.h"


FeatureFetcherService::FeatureFetcherService(
        PixelDeviceHistoryCassandraService* pixelDeviceHistoryCassandraService,
        CacheService<Offer>* offerCacheService,
        DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService,
        FeatureDeviceHistoryCassandraService* featureDeviceHistoryCassandraService) {

        this->pixelDeviceHistoryCassandraService  = pixelDeviceHistoryCassandraService;
        this->offerCacheService = offerCacheService;
        this->deviceFeatureHistoryCassandraService = deviceFeatureHistoryCassandraService;
        this->featureDeviceHistoryCassandraService = featureDeviceHistoryCassandraService;
}


std::vector<std::shared_ptr<Offer> > FeatureFetcherService::getOffers(std::set<int> offerIds) {
        std::vector<std::shared_ptr<Offer> > offers;
        for(int offerId :  offerIds) {
                auto offerPtr = offerCacheService->findByEntityId(offerId);
                assertAndThrow(offerPtr.get() != NULL);
                offers.push_back(offerPtr);
        }

        return offers;
}


std::vector<std::shared_ptr<PixelDeviceHistory> > FeatureFetcherService::
fetchAllDevicesHittingPixelsForOffers(std::vector<std::shared_ptr<Offer> > offers,
                                      int pixelHitRecencyInSecond) {

        std::vector<std::shared_ptr<PixelDeviceHistory> > allDevicesHittingThePixel;

        for(std::shared_ptr<Offer> offer :  offers) {
                auto pixelIds = offerPixelMapCacheService->findPixelIdsMappedToOfferId(offer->id);

                if(pixelIds.empty()) {

                        LOG(ERROR)<<"pixelIds is empty for offer : " + StringUtil::toStr(offer->id);
                        entityToModuleStateStats->addStateModuleForEntity("EXCEPTIOON_NO_PIXEL_FOR_OFFER"
                                                                          , "FeatureFetcherService",
                                                                          "offer" + StringUtil::toStr(offer->id));
                        continue;
                }

                for(int pixelId :  pixelIds) {
                        tbb::concurrent_hash_map<int, std::shared_ptr<Pixel> >::accessor accessor;
                        auto found = pixelCacheService->idToPixelMap->find(accessor, pixelId);
                        if (!found) {
                                entityToModuleStateStats->addStateModuleForEntity("EXCEPTION_DEAD_PIXEL",
                                                                                  "FeatureFetcherService",
                                                                                  "ALL");
                                LOG(ERROR)<< "dead pixel with id "<< pixelId;
                                continue;
                        }

                        std::shared_ptr<PixelDeviceHistory> pixelDeviceHistory =
                                pixelDeviceHistoryCassandraService->readDeviceHistoryOfPixel(
                                        accessor->second->uniqueKey,
                                        pixelHitRecencyInSecond);

                        if(pixelDeviceHistory->devicesHittingPixel->size() == 0) {
                                LOG(ERROR)<<"devicesHittingPixel is empty for this pixelUniqueKey : "<<pixelDeviceHistory->pixelUniqueKey;
                                entityToModuleStateStats->addStateModuleForEntity("EXCEPTION_NO_HISTORY_FOR_PIXEL",
                                                                                  "FeatureFetcherService",
                                                                                  "pixel" + StringUtil::toStr(pixelDeviceHistory->pixelUniqueKey));

                        }
                        allDevicesHittingThePixel.push_back(pixelDeviceHistory);
                        if (allDevicesHittingThePixel.size() >= 1000 ) {

                                //we don't to load too many devices
                                return allDevicesHittingThePixel;
                        }
                }
        }

        return allDevicesHittingThePixel;
}


void FeatureFetcherService::
collectFeaturesBasedOnTypesRequested(
        std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory,
        std::vector<std::shared_ptr<DeviceFeatureHistory> >& allDeviceFeatureHistories,
        std::shared_ptr<ModelRequest> model) {

        allDeviceFeatureHistories.push_back(deviceFeatureHistory);
}


std::vector<std::shared_ptr<DeviceFeatureHistory> > FeatureFetcherService::
fetchAllDeviceFeaturesFromPixelDeviceHistories(
        std::vector<std::shared_ptr<PixelDeviceHistory> > allDevicesHittingThePixel,
        std::shared_ptr<ModelRequest> modelDto) {

        std::vector<std::shared_ptr<DeviceFeatureHistory> > allDeviceFeatureHistories;

        for(std::shared_ptr<PixelDeviceHistory> pixelDeviceHistory : allDevicesHittingThePixel) {

                for(auto&& entry : *pixelDeviceHistory->devicesHittingPixel )
                {
                        MLOG(3)<<"device hit pixel at :  "<<entry.first<<", deviceId :  "
                               <<entry.second->getDeviceId()<<", deviceType: "<<entry.second->getDeviceType();

                        TimeType featureRecencyInSecond = modelDto->featureRecencyInSecond;

                        auto deviceHistory = std::make_shared<DeviceHistory>(
                                std::move(entry.second)
                                );

                        std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory =
                                deviceFeatureHistoryCassandraService->
                                readFeatureHistoryOfDevice(deviceHistory->device,
                                                           featureRecencyInSecond,
                                                           modelDto->featureTypesRequested,
                                                           modelDto->maxNumberOfHistoryPerDevice);

                        MLOG(3) << "size of feature loaded by this device : " << deviceFeatureHistory->getFeatures()->size();

                        collectFeaturesBasedOnTypesRequested(deviceFeatureHistory, allDeviceFeatureHistories, modelDto);
                        MLOG(3)<<"size of allDeviceFeatureHistories is  "<<allDeviceFeatureHistories.size()
                               <<" after adding histories";

                        //TODO make this property
                        if (allDeviceFeatureHistories.size() >= 1000 ) {

                                //we don't to load too many devices
                                break;
                        }
                }
        }
        return allDeviceFeatureHistories;
}
