
#include "PositiveFeatureFetcherInterface.h"
#include "FeatureFetcherService.h"
#include "CassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PositiveFeatureFetcher.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "ModelRequest.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "Offer.h"
#include "FeatureFetcherService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PositiveFeatureFetcher.h"
#include "PositiveFeatureFetcherInterface.h"
#include "ModelerContext.h"

#include "CollectionUtil.h"

#include <string>
#include <memory>
#include <boost/foreach.hpp>


PositiveFeatureFetcher::PositiveFeatureFetcher(
        std::shared_ptr<FeatureFetcherService> featureFetcherServiceArg){
        this->featureFetcherService =  featureFetcherServiceArg;
}



std::string PositiveFeatureFetcher::getName() {
        return "PositiveFeatureFetcher";
}

PositiveFeatureFetcher::~PositiveFeatureFetcher() {

}


void PositiveFeatureFetcher::process(std::shared_ptr<ModelerContext> context) {
        if (context->modelRequest->modelType.compare(ModelRequest::seedModel) == 0) {
                std::string seedWebsites = JsonArrayUtil::convertListToJson(
                        CollectionUtil::convertSetToList<std::string>(
                                context->modelRequest->seedWebsites));
                fetchPositveFeaturesBasedOnSeedModel(context);
                MLOG(3)<<"positiveDeviceHistories size :  "
                       <<context->positiveDeviceHistories->size()<<" for seedWebsites :"<<
                        seedWebsites;

        } else if (context->modelRequest->modelType.compare(ModelRequest::pixelModel) == 0) {
                fetchPositveFeaturesBasedOnPixelModel(context);

                MLOG(3)<<"positiveDeviceHistories size :  "<<
                        context->positiveDeviceHistories->size()
                       <<" for positiveOfferIds :"<<JsonArrayUtil::convertListToJson(
                        CollectionUtil::convertSetToList<int>(
                                context->modelRequest->positiveOfferIds
                                ));
        } else {
                throwEx("model type is wrong. type : " + context->modelRequest->modelType + ", request : "+ context->modelRequest->toJson());
        }


}


void PositiveFeatureFetcher::fetchPositveFeaturesBasedOnPixelModel(std::shared_ptr<ModelerContext> context) {


        std::vector<std::shared_ptr<Offer> > offers = featureFetcherService->getOffers(context->modelRequest->positiveOfferIds);

        MLOG(2)<< " started fetching allDevicesHittingThePixel ";
        std::vector<std::shared_ptr<PixelDeviceHistory> > allDevicesHittingThePixel  =
                featureFetcherService->fetchAllDevicesHittingPixelsForOffers(
                        offers,
                        context->modelRequest->pixelHitRecencyInSecond);
        MLOG(2)<< " fetched "<<allDevicesHittingThePixel.size()<<" allDevicesHittingThePixel ";
        if (allDevicesHittingThePixel.empty()) {
                context->redFlags.push_back("there are no devices hitting pixel for positive pixels");
        }

        MLOG(2)<< " started fetching fetchAllDeviceFeaturesFromPixelDeviceHistories ";
        std::vector<std::shared_ptr<DeviceFeatureHistory> > allDeviceFeatureHistories  =
                featureFetcherService->fetchAllDeviceFeaturesFromPixelDeviceHistories(allDevicesHittingThePixel, context->modelRequest);
        MLOG(2)<< " allDeviceFeatureHistories size " <<allDeviceFeatureHistories.size();

        for(std::shared_ptr<DeviceFeatureHistory> devFeatureHistory : allDeviceFeatureHistories) {
                auto alldevFeatureHistory = devFeatureHistory->getFeaturesHappenedMoreThanNTimes(2);


                devFeatureHistory->positiveHistoryValue = true;
                context->positiveDeviceHistories->push_back(devFeatureHistory);
        }
        MLOG(2)<< " cleaned up all features";

        context->modelRequest->modelResult->numberOfPositiveDevicesUsed += allDeviceFeatureHistories.size();

        context->modelRequest->numberOfPositiveDevicesToBeUsed = allDeviceFeatureHistories.size();
        if (context->positiveDeviceHistories->empty()) {
                context->redFlags.push_back("unable to fetch positive device histories");
                return;
        }
}


void PositiveFeatureFetcher::fetchPositveFeaturesBasedOnSeedModel(
        std::shared_ptr<ModelerContext> context) {
        if (context->modelRequest->seedWebsites.empty()) {
                LOG(ERROR) << "seedWebsites filed is empty...aborting";
                context->redFlags.push_back("seedWebsites filed is empty.");
                return;
        }

        std::vector<std::shared_ptr<FeatureDeviceHistory> > positiveFeatures =
                featureDeviceHistoryCassandraService->readDeviceHistoryOfFeatures(
                        context->modelRequest->numberOfPositiveDevicesToBeUsed,
                        context->modelRequest->featureRecencyInSecond,
                        context->modelRequest->seedWebsites,
                        Feature::generalTopLevelDomain);

        MLOG(3)<<"positiveFeatures size :  "<<
                positiveFeatures.size()<<" is for seed : "<<JsonArrayUtil::convertListToJson(
                CollectionUtil::convertSetToList<std::string>(
                        context->modelRequest->seedWebsites));

        if(positiveFeatures.empty()) {
                LOG(ERROR) << "didn't find any positiveFeatures for seedWebsites : " << JsonArrayUtil::convertListToJson(context->modelRequest->seedWebsites);
                context->redFlags.push_back("cannot build model because positiveFeatures is none");
        }


        std::vector<std::shared_ptr<DeviceFeatureHistory> > positiveDeviceHistories;
        std::set<std::string> allDeviceFeatureHistoriesFound;
        std::set<std::string> allPositiveDevicesLookedUp;
        for (auto positiveFeature : positiveFeatures) {
                long sumOfAllDevicesUsed = 0;
                for (auto positiveDevice : positiveFeature->devicehistories) {
                        auto positiveHistoryOfDevice =
                                deviceFeatureHistoryCassandraService->readFeatureHistoryOfDevice(
                                        positiveDevice->device,
                                        context->modelRequest->featureRecencyInSecond,
                                        context->modelRequest->featureTypesRequested,
                                        context->modelRequest->maxNumberOfHistoryPerDevice);

                        allPositiveDevicesLookedUp.insert(positiveDevice->toJson());
                        allDeviceFeatureHistoriesFound.insert(positiveDevice->toJson());
                        auto alldevFeatureHistory = positiveHistoryOfDevice->getFeaturesHappenedMoreThanNTimes(2);

                        positiveDeviceHistories.push_back(positiveHistoryOfDevice);

                        sumOfAllDevicesUsed++;

                        context->modelRequest->modelResult->numberOfPositiveDevicesUsed++;

                        if (context->modelRequest->modelResult->numberOfPositiveDevicesUsed
                            >= context->modelRequest->numberOfPositiveDevicesToBeUsed) {
                                MLOG(3)<<
                                        "reached the limit of positive devices used for modeling :"<<
                                        context->modelRequest->numberOfPositiveDevicesToBeUsed;
                                break;
                        }
                }


                if(!positiveFeature->devicehistories.empty()) {

                        long avgDevicesUsedForFeature =
                                sumOfAllDevicesUsed / positiveFeature->devicehistories.size();

                        context->modelRequest->modelResult->featureToAvgNumberOfHistoryUsed
                        .insert(std::pair<std::string, int>
                                        (positiveFeature->feature->getName(), avgDevicesUsedForFeature));

                }



        }

        for (auto ptr : positiveDeviceHistories) {
                ptr->positiveHistoryValue = true;
                context->positiveDeviceHistories->push_back(ptr);
        }


        MLOG(3)<<"allPositiveDevicesLookedUp : "<<JsonArrayUtil::convertListToJson(allPositiveDevicesLookedUp);
        MLOG(3)<<"allDeviceFeatureHistoriesFound : "<<JsonArrayUtil::convertListToJson(allDeviceFeatureHistoriesFound);
        MLOG(3)<<"positiveDeviceHistories.size() : "<<positiveDeviceHistories.size();

        if(context->positiveDeviceHistories->empty()) {
                context->redFlags.push_back("unable to fetch positive device histories");
                return;
        }
}
