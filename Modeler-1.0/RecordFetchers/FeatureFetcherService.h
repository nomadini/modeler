/*
 * FeatureFetcher.h
 *
 *  Created on: Oct 22, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_MODELER_FEATUREFETCHER_UTIL_H_
#define GICAPODS_MODELER_FEATUREFETCHER_UTIL_H_

class PixelDeviceHistoryCassandraService;

template<class T>
class CacheService;

class ModelRequest;
class OfferPixelMapCacheService;
class EntityToModuleStateStats;
class DeviceFeatureHistory;
#include <memory>
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"

#include <string>
#include <set>
#include <set>
#include  <tbb/concurrent_hash_map.h>
class PixelCacheService;
class PixelDeviceHistory;
class Pixel;
class Offer;


class FeatureFetcherService {

public:
EntityToModuleStateStats* entityToModuleStateStats;
PixelDeviceHistoryCassandraService* pixelDeviceHistoryCassandraService;
DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService;
FeatureDeviceHistoryCassandraService* featureDeviceHistoryCassandraService;
OfferPixelMapCacheService* offerPixelMapCacheService;
CacheService<Offer>* offerCacheService;
PixelCacheService* pixelCacheService;

FeatureFetcherService(PixelDeviceHistoryCassandraService* pixelDeviceHistoryCassandraService,
                      CacheService<Offer>* offerCacheService,
                      DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService,
                      FeatureDeviceHistoryCassandraService* featureDeviceHistoryCassandraService);

virtual std::vector<std::shared_ptr<Offer> > getOffers(std::set<int> offerIds);

virtual std::vector<std::shared_ptr<DeviceFeatureHistory> >
fetchAllDeviceFeaturesFromPixelDeviceHistories(
        std::vector<std::shared_ptr<PixelDeviceHistory> > allDevicesHittingThePixel,
        std::shared_ptr<ModelRequest> modelDto);

virtual std::vector<std::shared_ptr<PixelDeviceHistory> >
fetchAllDevicesHittingPixelsForOffers(
        std::vector<std::shared_ptr<Offer> > offers,
        int pixelHitRecencyInSecond);

void collectFeaturesBasedOnTypesRequested(
        std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory,
        std::vector<std::shared_ptr<DeviceFeatureHistory> >&allDeviceFeatureHistories,
        std::shared_ptr<ModelRequest> model);
};

#endif /* GICAPODS_MODELER_FEATUREFETCHER_UTIL_H_ */
