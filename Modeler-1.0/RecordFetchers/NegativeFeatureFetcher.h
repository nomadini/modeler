/*
 * FeatureFetcher.h
 *
 *  Created on: Oct 22, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_MODELER_NegativeFeatureFetcher_H
#define GICAPODS_MODELER_NegativeFeatureFetcher_H


#include "NegativeFeatureFetcherInterface.h";
#include "ModelerModule.h";
class FeatureFetcherService;
class PixelModelNegativeFeatureFetcher;

#include "FeatureDeviceHistoryCassandraService.h";
#include "DeviceFeatureHistoryCassandraService.h";
class ModelerContext;
class EntityToModuleStateStats;
class NegativeFeatureFetcher : public NegativeFeatureFetcherInterface, public ModelerModule {


public:
EntityToModuleStateStats* entityToModuleStateStats;

PixelModelNegativeFeatureFetcher* pixelModelNegativeFeatureFetcher;
DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService;
FeatureDeviceHistoryCassandraService* featureDeviceHistoryCassandraService;

NegativeFeatureFetcher();
virtual void fetchNegativeFeaturesBasedOnSeedModel(std::shared_ptr<ModelerContext> context);
virtual void process(std::shared_ptr<ModelerContext> context);

virtual void fetchNegativeFeaturesBasedOnPixelModel(std::shared_ptr<ModelerContext> context);

static void populateNegativeDeviceHistoryWithFeatureDeviceHistories(
        std::shared_ptr<ModelerContext> context,
        std::vector<std::shared_ptr<FeatureDeviceHistory> > featureDeviceHistories,
        DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService,
        FeatureDeviceHistoryCassandraService* featureDeviceHistoryCassandraService);


std::string getName();
virtual ~NegativeFeatureFetcher();

};

#endif /* GICAPODS_MODELER_FEATUREFETCHER_H */
