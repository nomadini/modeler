/*
 * FeatureFetcher.h
 *
 *  Created on: Oct 22, 2015
 *      Author: mtaabodi
 */

#ifndef PixelModelPixelModelNegativeFeatureFetcher_H
#define PixelModelPixelModelNegativeFeatureFetcher_H

class FeatureFetcherService;
class PixelDeviceHistory;

#include "FeatureDeviceHistoryCassandraService.h";
#include "DeviceFeatureHistoryCassandraService.h";
class ModelerContext;
class EntityToModuleStateStats;
class PixelModelNegativeFeatureFetcher {


public:
std::shared_ptr<FeatureFetcherService> featureFetcherService;
EntityToModuleStateStats* entityToModuleStateStats;

DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService;
FeatureDeviceHistoryCassandraService* featureDeviceHistoryCassandraService;

PixelModelNegativeFeatureFetcher(std::shared_ptr<FeatureFetcherService> featureFetcherServiceArg);

virtual void fetchNegativeFeaturesBasedOnPixelModel(std::shared_ptr<ModelerContext> context);

void fetchNegativeDevicesBasedOnPixelHitDevices(std::shared_ptr<ModelerContext> context,
                                                std::vector<std::shared_ptr<PixelDeviceHistory> > allDevicesHittingThePixel);

void fetchNegativeDevicesBasedOnRandomDevices(std::shared_ptr<ModelerContext> context);

virtual ~PixelModelNegativeFeatureFetcher();

};

#endif /* GICAPODS_MODELER_FEATUREFETCHER_H */
