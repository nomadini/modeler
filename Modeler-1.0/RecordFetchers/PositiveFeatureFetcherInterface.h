/*
 * PositiveFeatureFetcher.h
 *
 *  Created on: Oct 22, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_POSITIVEFEATUREFETCHER_INTERFACE_H
#define GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_POSITIVEFEATUREFETCHER_INTERFACE_H

class StringUtil;
class ModelerContext;
#include <memory>
#include <string>
#include <set>
#include <vector>

class PositiveFeatureFetcherInterface {

public:

virtual void fetchPositveFeaturesBasedOnSeedModel(std::shared_ptr<ModelerContext> context)=0;

virtual void fetchPositveFeaturesBasedOnPixelModel(std::shared_ptr<ModelerContext> context)=0;

virtual ~PositiveFeatureFetcherInterface() {
}
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_POSITIVEFEATUREFETCHER_INTERFACE_H */
