/*
 * FeatureFetcher.h
 *
 *  Created on: Oct 22, 2015
 *      Author: mtaabodi
 */

#include "ModelerContext.h"
#include "FeatureFetcherService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PixelDeviceHistory.h"
#include "NegativeFeatureFetcher.h"
#include "PixelModelNegativeFeatureFetcher.h"

#include "ModelRequest.h"
#include "Offer.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "CollectionUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "FeatureFetcherService.h"

#include "FeatureFetcherService.h";


PixelModelNegativeFeatureFetcher::PixelModelNegativeFeatureFetcher(
								std::shared_ptr<FeatureFetcherService > featureFetcherServiceArg) {
								this->featureFetcherService =  featureFetcherServiceArg;
}



PixelModelNegativeFeatureFetcher::~PixelModelNegativeFeatureFetcher() {

}


void PixelModelNegativeFeatureFetcher::fetchNegativeFeaturesBasedOnPixelModel(
								std::shared_ptr<ModelerContext> context) {
								std::vector<std::shared_ptr<Offer> > offers = featureFetcherService->getOffers(context->modelRequest->negativeOfferIds);
								//offers can be empty for negative pixels. this is very valid

								std::vector<std::shared_ptr<PixelDeviceHistory> > allDevicesHittingThePixel  =
																featureFetcherService->fetchAllDevicesHittingPixelsForOffers(
																								offers,
																								context->modelRequest->pixelHitRecencyInSecond);

								if(!allDevicesHittingThePixel.empty()) {
																fetchNegativeDevicesBasedOnPixelHitDevices(context, allDevicesHittingThePixel);
								} else {

																//User has not selected a set of negative offers
																//we should find random negative websites now as
																fetchNegativeDevicesBasedOnRandomDevices(context);
								}


}

void PixelModelNegativeFeatureFetcher::fetchNegativeDevicesBasedOnRandomDevices(
								std::shared_ptr<ModelerContext> context) {
								std::set<std::string> empty;
								int maxNumberOfNegativeFeaturesToPick  = context->modelRequest->maxNumberOfNegativeFeaturesToPick;
								if (maxNumberOfNegativeFeaturesToPick <= 10) {
																maxNumberOfNegativeFeaturesToPick = 100;
								}

								auto featureDeviceHistories =
																featureDeviceHistoryCassandraService->
																readDistinctFeaturesExcluding(
																								empty,
																								maxNumberOfNegativeFeaturesToPick,
																								context->modelRequest->featureRecencyInSecond,
																								context->modelRequest->featureTypesRequested);

								NegativeFeatureFetcher::populateNegativeDeviceHistoryWithFeatureDeviceHistories(
																context,
																featureDeviceHistories,
																deviceFeatureHistoryCassandraService,
																featureDeviceHistoryCassandraService);
}

void PixelModelNegativeFeatureFetcher::fetchNegativeDevicesBasedOnPixelHitDevices(
								std::shared_ptr<ModelerContext> context,
								std::vector<std::shared_ptr<PixelDeviceHistory> > allDevicesHittingThePixel) {
								std::vector<std::shared_ptr<DeviceFeatureHistory> > allDeviceFeatureHistories  =
																featureFetcherService->fetchAllDeviceFeaturesFromPixelDeviceHistories(
																								allDevicesHittingThePixel,
																								context->modelRequest);

								for(std::shared_ptr<DeviceFeatureHistory> devFeatureHistory : allDeviceFeatureHistories) {
																auto alldevFeatureHistory = devFeatureHistory->getFeaturesHappenedMoreThanNTimes(3);
																context->negativeDeviceHistories->push_back(devFeatureHistory);
								}

								if (context->negativeDeviceHistories->empty()) {
																context->redFlags.push_back("unable to fetch negative device histories");
																return;
								}

								context->modelRequest->modelResult->numberOfNegativeDevicesUsed = allDeviceFeatureHistories.size();
}
