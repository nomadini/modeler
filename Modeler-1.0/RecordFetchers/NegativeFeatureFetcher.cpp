/*
 * FeatureFetcher.h
 *
 *  Created on: Oct 22, 2015
 *      Author: mtaabodi
 */

#include "ModelerContext.h"
#include "NegativeFeatureFetcherInterface.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "NegativeFeatureFetcher.h"
#include "PixelModelNegativeFeatureFetcher.h"

#include "ModelRequest.h"
#include "Offer.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "CollectionUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "NegativeFeatureFetcherInterface.h"
NegativeFeatureFetcher::NegativeFeatureFetcher() {
}

void NegativeFeatureFetcher::process(std::shared_ptr<ModelerContext> context) {

								if (context->modelRequest->modelType.compare(ModelRequest::seedModel) == 0) {
																assertAndThrow(context->modelRequest->seedWebsites.size() != 0);
																fetchNegativeFeaturesBasedOnSeedModel(context);

																std::string seedWebsites = JsonArrayUtil::convertListToJson(
																								CollectionUtil::convertSetToList<std::string>(
																																context->modelRequest->seedWebsites));

																MLOG(3)<<"negativeDeviceHistories size :  "<<
																								context->negativeDeviceHistories->size()
																							<<" for seedWebsites :"<<seedWebsites;

								} else if (context->modelRequest->modelType.compare(ModelRequest::pixelModel) == 0) {

																fetchNegativeFeaturesBasedOnPixelModel(context);

																MLOG(3)<<"negativeDeviceHistories size :  "<<
																								context->negativeDeviceHistories->size()<<" for negativeOfferIds :"<<
																								JsonArrayUtil::convertListToJson(
																								CollectionUtil::convertSetToList<int>(context->modelRequest->negativeOfferIds));

								} else {
																throwEx("model type is wrong. type : " + context->modelRequest->modelType + ", request : "+ context->modelRequest->toJson());
								}

}

void NegativeFeatureFetcher::fetchNegativeFeaturesBasedOnPixelModel(std::shared_ptr<ModelerContext> context) {
								pixelModelNegativeFeatureFetcher->fetchNegativeFeaturesBasedOnPixelModel(context);
}


void NegativeFeatureFetcher::
fetchNegativeFeaturesBasedOnSeedModel(std::shared_ptr<ModelerContext> context) {


								int maxNumberOfNegativeFeaturesToPick  = context->modelRequest->maxNumberOfNegativeFeaturesToPick;
								if (maxNumberOfNegativeFeaturesToPick <= 10) {
																maxNumberOfNegativeFeaturesToPick = 100;
								}

								std::vector<std::shared_ptr<FeatureDeviceHistory> > featureDeviceHistories;
								if (context->modelRequest->negativeFeaturesRequested.empty()) {
																featureDeviceHistories =
																								featureDeviceHistoryCassandraService->
																								readDistinctFeaturesExcluding(
																																context->modelRequest->seedWebsites,
																																maxNumberOfNegativeFeaturesToPick,
																																context->modelRequest->featureRecencyInSecond,
																																context->modelRequest->featureTypesRequested);

								} else {
																featureDeviceHistories =
																								featureDeviceHistoryCassandraService->
																								readDistinctFeaturesInThisSet(
																																context->modelRequest->negativeFeaturesRequested,
																																context->modelRequest->featureRecencyInSecond,
																																context->modelRequest->featureTypesRequested);
								}

								if (featureDeviceHistories.empty()) {
																LOG(WARNING) << "couldnt load any negative features for request " <<context->modelRequest->toJson();
																context->redFlags.push_back("unable to fetch negative device histories");
								}

								populateNegativeDeviceHistoryWithFeatureDeviceHistories(
																context,
																featureDeviceHistories,
																deviceFeatureHistoryCassandraService,
																featureDeviceHistoryCassandraService
																);

}

void NegativeFeatureFetcher::populateNegativeDeviceHistoryWithFeatureDeviceHistories(
								std::shared_ptr<ModelerContext> context,
								std::vector<std::shared_ptr<FeatureDeviceHistory> > featureDeviceHistories,
								DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService,
								FeatureDeviceHistoryCassandraService* featureDeviceHistoryCassandraService) {
								std::vector<std::shared_ptr<DeviceFeatureHistory> > deviceFeatureHistories;

								for (auto featureDeviceHistory : featureDeviceHistories) {
																if (!featureDeviceHistory->feature->isValid()) {
																								LOG(ERROR)<< "ignoring bad feature";
																								continue;

																}

																long sumOfAllDevicesUsed = 0;

																featureDeviceHistory =
																								featureDeviceHistoryCassandraService->
																								readDeviceHistoryOfFeature(
																																featureDeviceHistory->feature->getName(),
																																featureDeviceHistory->feature->getType(),
																																(TimeType) context->modelRequest->featureRecencyInSecond,
																																context->modelRequest->maxNumberOfDeviceHistoryPerFeature
																																);

																MLOG(3)<<"featureDeviceHistory has  "
																							<<featureDeviceHistory->devicehistories.size()<<" deviceHistories";
																if (featureDeviceHistory->devicehistories.size() <= 10) {
																								LOG_EVERY_N(WARNING, 10) <<google::COUNTER<<
																																"th"<< featureDeviceHistory->feature->getName()<< " is not inclueded. doesn't have enough device history to be meaniningful : "<< featureDeviceHistory->devicehistories.size();
																								continue;
																}
																for (auto deviceHistory : featureDeviceHistory->devicehistories) {

																								std::shared_ptr<DeviceFeatureHistory> negativeHistoryOfDevice =
																																deviceFeatureHistoryCassandraService->
																																readFeatureHistoryOfDevice(
																																								deviceHistory->device,
																																								context->modelRequest->featureRecencyInSecond,
																																								context->modelRequest->featureTypesRequested,
																																								context->modelRequest->maxNumberOfHistoryPerDevice);
																								sumOfAllDevicesUsed++;
																								MLOG(3)<<"featureDeviceHistories histories read is : "<<negativeHistoryOfDevice->getFeatures()->size();

																								deviceFeatureHistories.push_back(negativeHistoryOfDevice);
																								context->modelRequest->modelResult->numberOfNegativeDevicesUsed += sumOfAllDevicesUsed;
																								if (context->modelRequest->numberOfNegativeDevicesToBeUsed >= context->modelRequest->modelResult->numberOfNegativeDevicesUsed)
																								{
																																MLOG(3)<<"reached the limit of featureDeviceHistories devices used for modelling : "<<context->modelRequest->numberOfNegativeDevicesToBeUsed;
																																break;
																								}
																}

																long avgDevicesUsedForFeature = sumOfAllDevicesUsed / featureDeviceHistory->devicehistories.size();


																context->modelRequest->modelResult->featureToAvgNumberOfHistoryUsed
																.insert(std::pair<std::string, int>
																																(featureDeviceHistory->feature->getName(), avgDevicesUsedForFeature));
								}

								for (auto ptr : deviceFeatureHistories) {
																ptr->positiveHistoryValue = false;
																context->negativeDeviceHistories->push_back(ptr);
								}
								MLOG(3)<<"deviceFeatureHistories.size() :"<<
																deviceFeatureHistories.size();

								if(context->negativeDeviceHistories->empty()) {
																context->redFlags.push_back("unable to fetch negative device histories");
																return;
								}
}




NegativeFeatureFetcher::~NegativeFeatureFetcher() {

}

std::string NegativeFeatureFetcher::getName() {
								return "NegativeFeatureFetcher";
}
