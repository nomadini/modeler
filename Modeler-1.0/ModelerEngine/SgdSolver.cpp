
#include "GUtil.h"
#include <shogun/structure/MultilabelModel.h>
#include <shogun/classifier/svm/SVMSGD.h>
#include <shogun/structure/StochasticSOSVM.h>
#include <shogun/structure/MultilabelSOLabels.h>
#include <shogun/structure/DualLibQPBMSOSVM.h>

#include <shogun/structure/PrimalMosekSOSVM.h>

#include "SgdSolver.h"

//not used by any one but kept here for sample
CBinaryLabels* SgdSolver::createLabelsFromFile(std::string fname_labels) {
								/* labels from vector */
								CCSVFile* label_file = new CCSVFile(fname_labels.c_str());
								SGVector<float64_t> label_vec;
								label_vec.load(label_file);
								SG_UNREF(label_file);
								MLOG(3)<<"label_vec size is : "<<label_vec.size();
								CBinaryLabels* labels = new CBinaryLabels(label_vec);
								SG_REF(labels);
								return labels;
}
/**
 * not used by any one but kept here for sample
 * this reads a dense feature file
 */
CDotFeatures* SgdSolver::createFeaturesFromFile(std::string fname_feats) {
								/* dense features from matrix */
								CLibSVMFile * file = new CLibSVMFile(fname_feats.c_str());
								ASSERT(file != NULL); SG_REF(file);

								SGSparseVector<float64_t> * feats;
								SGVector<float64_t> * labels;
								int32_t dim_feat, num_samples, num_classes;
								file->get_sparse_matrix(feats, dim_feat, num_samples, labels,
																																num_classes);

								//	num_samples = 100;
								SGMatrix<float64_t> mat = SGMatrix<float64_t>();
								mat = SGMatrix<float64_t>(dim_feat, num_samples);
								std::cout << "matrix loaded from svm file : " << dim_feat << std::endl;
								std::cout << "dim_feat : " << dim_feat << std::endl;
								std::cout << "num_samples : " << num_samples << std::endl;

								for (int32_t i = 0; i < num_samples; i++) {

																for (int32_t j = 0; j < dim_feat; j++) {
																								std::cout << mat(j, i) << " ";

																}
																std::cout << std::endl;
								}

								CDenseFeatures<float64_t> *features =
																new CDenseFeatures<float64_t>(mat);
								SG_REF(features);
								return (CDotFeatures*) features;
}

std::vector<float64_t> SgdSolver::createSvmSgdModel(
								CDotFeatures* features,
								CBinaryLabels* labels) {
								// if (!labels->ensure_valid()) {
								//         LOG(ERROR)<< "labels are not valid";
								// }
								if(labels == NULL) {
																throwEx("labels are null");
								}
								if(features == NULL) {
																throwEx("features are null");
								}

								int constant = 100; //the higher it is, the bigger is the difference between
								//the good features and bad features
								CSVMSGD* svmSgd = new CSVMSGD(
																constant,
																(CDotFeatures*) features,
																labels);
								MLOG(1)<<"started training for model";
								svmSgd->train();
								MLOG(1)<<"finished training for model";
								SGVector<float64_t> weights = svmSgd->get_w();
								if (weights.size() == 0) {
																throwEx("modeler returned no weights");
								}
								std::vector<float64_t> weightsVector;

								for (int i = 0; i < weights.size(); i++) {
																weightsVector.push_back(weights.get_element(i));
																if (weights.get_element(i) != weightsVector.at(i)) {
																								throwEx("error in assignemnt");
																}
								}
								MLOG(3)<<"feature weight size is :  "<<weightsVector.size();

								delete svmSgd;
								return weightsVector;
}


// not used by any one but kept here for sample
void SgdSolver::load_data(const char * file_name, SGMatrix<float64_t> &feats_matrix,
																										int32_t &dim_feat, int32_t &num_samples,
																										SGVector<int32_t> * &multilabels, int32_t &num_classes) {
								CLibSVMFile * file = new CLibSVMFile(file_name);
								ASSERT(file != NULL); SG_REF(file);

								SGSparseVector<float64_t> * feats;
								SGVector<float64_t> * labels;

								file->get_sparse_matrix(feats, dim_feat, num_samples, labels,
																																num_classes);

								//	num_samples = 100;

								feats_matrix = SGMatrix<float64_t>(dim_feat, num_samples);
								std::cout << "dim_feat : " << dim_feat << std::endl;
								std::cout << "num_samples : " << num_samples << std::endl;

								/** preparation of data for multilabel model */
								for (index_t i = 0; i < num_samples; i++) {
																SGSparseVector<float64_t> feat_sample = feats[i];

																for (index_t j = 0; j < dim_feat; j++) {
																								//	std::cout<<"i, j is "<<i<<" ,"<<j<<"  value "<<feat_sample.get_feature(j)<<std::endl;
																								int ind_mat = (i * dim_feat) + j;
																								feats_matrix(i, j) = feat_sample.get_feature(j);
																}
								}

								multilabels = SG_MALLOC(SGVector<int32_t>, num_samples);

								for (index_t i = 0; i < num_samples; i++) {
																SGVector<float64_t> label_sample = labels[i];
																SGVector<int32_t> multilabel_sample(label_sample.vlen);

																for (index_t j = 0; j < label_sample.vlen; j++) {
																								multilabel_sample[j] = label_sample[j];
																}

																//	multilabel_sample.qsort();

																multilabels[i] = multilabel_sample;
								}

								SG_UNREF(file);
								SG_FREE(feats);
								SG_FREE(labels);
}

/**
 * this will create stocastic sgd so model
 */
SGVector<float64_t> SgdSolver::createStochasticSOSVMModel(
								std::string trainFileName) {

								SGMatrix<float64_t> feats_matrix;
								SGVector<int32_t> * multilabels;
								int32_t dim_feat;
								int32_t num_samples;
								int32_t num_classes;

								load_data(trainFileName.c_str(), feats_matrix, dim_feat, num_samples,
																		multilabels, num_classes);

								SG_SPRINT("Number of samples    =  %d\n", num_samples);
								SG_SPRINT("Dimension of feature =  %d\n", dim_feat);
								SG_SPRINT("Number of classes    =  %d\n", num_classes);

								SG_SPRINT("-------------------------------------------\n");

								CMultilabelSOLabels * mlabels = new CMultilabelSOLabels(num_samples,
																																																																num_classes);
								SG_REF(mlabels);
								mlabels->set_sparse_labels(multilabels);

								CSparseFeatures<float64_t> * features = new CSparseFeatures<float64_t>(
																feats_matrix);
								SG_REF(features);

								CMultilabelModel * model = new CMultilabelModel(features, mlabels);
								SG_REF(model);

								CStochasticSOSVM * sgd = new CStochasticSOSVM(model, mlabels);
								SG_REF(sgd);

								CDualLibQPBMSOSVM * bundle = new CDualLibQPBMSOSVM(model, mlabels, 100);
								bundle->set_verbose(false);
								SG_REF(bundle);

//		CPrimalMosekSOSVM * sosvm = new CPrimalMosekSOSVM(model, mlabels);
//		SG_REF(sosvm);

								CTime * start = new CTime();
								SG_REF(start);
								sgd->train();
								float64_t t1 = start->cur_time_diff(false);
								bundle->train();
								float64_t t2 = start->cur_time_diff(false);
								//sosvm->train();
								float64_t t3 = start->cur_time_diff(false);

								SG_SPRINT(">>> Time taken for training using %s = %f\n",
																		sgd->get_name(), t1);
								SG_SPRINT(">>> Time taken for training using %s = %f\n",
																		bundle->get_name(), t2 - t1);
//		SG_SPRINT(">>> Time taken for learning using %s = %f\n", sosvm->get_name(),
//		          t3 - t2);

								return sgd->get_w();
}


SGVector<float64_t> SgdSolver::processStochasticSOSVM(
								std::string featuresFileName,
								std::string labelsFileName) {
								//creates the model based on libSvm data , stochasti multi class data
								SGVector<float64_t> weights =
																createStochasticSOSVMModel(
																								featuresFileName);

								return weights;
}
