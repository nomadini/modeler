/*
 * FeatureValidator.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef FeatureValidator_H_
#define FeatureValidator_H_

class Device;

#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#include "ModelerModule.h"
#include <shogun/lib/common.h>

class FeatureValidator {

public:
FeatureValidator();

void validateFeatures(std::vector<float64_t> featureWeights,
																						std::unordered_map<std::string, int> featureIndexMap);

};

#endif
