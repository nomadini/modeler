
#include "FeatureWeightToModelConverter.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "ModelUtil.h"
#include "ModelRequest.h"
FeatureWeightToModelConverter::FeatureWeightToModelConverter()  {

}
void FeatureWeightToModelConverter::process(std::shared_ptr<ModelerContext> context) {

}

std::string FeatureWeightToModelConverter::getName() {
								return "FeatureWeightToModelConverter";
}

void FeatureWeightToModelConverter::createModelFromFeatureWeights(
								std::vector<float64_t> featureWeights,
								std::unordered_map<int, std::string> indexFeatureMap,
								std::shared_ptr<ModelRequest> model) {

								model->modelResult->featureScoreMap = ModelUtil::buildFeatureScoreMap(
																indexFeatureMap, featureWeights);

								assertAndThrow(!model->modelResult->featureScoreMap.empty());

								model->modelResult->topFeatureScoreMap =
																ModelUtil::getHighestFeatureScoreMap
																								(model->modelResult->featureScoreMap,
																								model->numberOfTopFeatures,
																								model->cutOffScore); //cutOffScoreFeatureScoreMapInclusion instead of 3.00

								assertAndWarn(model->modelResult->topFeatureScoreMap.size()>0);
								if(model->modelResult->topFeatureScoreMap.size() == 0) {
																MLOG(3)<<"seems like no top feature made it above the model cutOffScore , "<<model->cutOffScore;
								}
}
