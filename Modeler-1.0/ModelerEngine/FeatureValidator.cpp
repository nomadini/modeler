/*
 * FeatureValidator.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */


#include "Device.h"

#include "GUtil.h"


#include "ModelUtil.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "NegativeFeatureFetcher.h"
#include "ModelRequest.h"
#include "DeviceFeatureHistoryTestHelper.h"
#include "PositiveFeatureFetcher.h"
#include "FeatureValidator.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>

FeatureValidator::FeatureValidator() {
}

void FeatureValidator::validateFeatures(std::vector<float64_t> featureWeights,
                                        std::unordered_map<std::string, int> featureIndexMap) {
        if(featureWeights.size() == 0) {
                throwEx("feature weight cannot be zero");
        }
        if (featureWeights.size() != featureIndexMap.size()) {
                //this is just a sanity check
                LOG(WARNING)<<"features weights size  :  "<<featureWeights.size()
                            <<" , is wrong, should be equal to featureIndexMap size:"<<
                        featureIndexMap.size();
        }

        std::unordered_map<int, std::string> indexFeatureMap =
                ModelUtil::getIndexFeatureMap(featureIndexMap);

        if (featureIndexMap.size() != indexFeatureMap.size()) {

                MLOG(3)<<"indexFeatureMap.size() :  "<<indexFeatureMap.size()
                       <<" , featureIndexMap.size() : " <<featureIndexMap.size();
                throwEx(
                        "indexFeatureMap and featureIndexMap should have equal size");
        }
}
