/*
 * SgdSolver.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef SGDSOLVER_H_
#define SGDSOLVER_H_

#include <shogun/features/SparseFeatures.h>
#include <shogun/lib/config.h>
#include <shogun/io/SGIO.h>
#include <shogun/io/CSVFile.h>
#include <shogun/structure/MultilabelModel.h>
#include <shogun/structure/MultilabelSOLabels.h>
#include <shogun/structure/DualLibQPBMSOSVM.h>
#include <shogun/lib/Time.h>
#include <shogun/structure/PrimalMosekSOSVM.h>
#include <shogun/io/LibSVMFile.h>

#include "JsonTypeDefs.h"

#include <memory>
#include <string>
#include <vector>
#include <set>
class EntityToModuleStateStats;

using namespace shogun;

class SgdSolver {
//this class needs a feature file and a label file
//outputs the featureWeight file
private:
/**
 * this reads from a label only file
 */
CBinaryLabels* createLabelsFromFile(std::string fname_labels);
/**
 * this reads a dense feature file
 */
CDotFeatures* createFeaturesFromFile(std::string fname_feats);

void load_data(const char * file_name, SGMatrix<float64_t> &feats_matrix,
															int32_t &dim_feat, int32_t &num_samples,
															SGVector<int32_t> * &multilabels, int32_t &num_classes);
public:
/**
 * this will create stocastic sgd so model
 */
SGVector<float64_t> createStochasticSOSVMModel(std::string trainFileName);

std::vector<float64_t> createSvmSgdModel(
								CDotFeatures* features,
								CBinaryLabels* labels);

SGVector<float64_t> processStochasticSOSVM(
								std::string featuresFileName,
								std::string labelsFileName);


EntityToModuleStateStats* entityToModuleStateStats;
};


#endif /* SGDSOLVER_H_ */
