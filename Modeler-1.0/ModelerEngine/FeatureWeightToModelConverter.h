/*
 * FeatureWeightToModelConverter.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef FeatureWeightToModelConverter_H_
#define FeatureWeightToModelConverter_H_

#include "ModelerModule.h"

class ModelRequest;
class ModelerContext;

#include <shogun/lib/common.h>
class FeatureWeightToModelConverter : public ModelerModule {
public:

FeatureWeightToModelConverter();

void createModelFromFeatureWeights(
								std::vector<float64_t> featureWeights,
								std::unordered_map<int, std::string> indexFeatureMap,
								std::shared_ptr<ModelRequest> modelRequest);

void process(std::shared_ptr<ModelerContext>  context);

std::string getName();
};

#endif /* FeatureWeightToModelConverter_H_ */
