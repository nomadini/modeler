/*
 * ModelDataCleaner.h
 *
 *  Created on: Dec 13, 2017
 *      Author: mtaabodi
 */

#ifndef ModelDataCleaner_H_
#define ModelDataCleaner_H_

class Device;
class ModelerContext;

class DeviceFeatureHistoryTestHelper;
class DeviceFeatureHistory;
class EntityToModuleStateStats;

#include "PositiveFeatureFetcherInterface.h"
#include <unordered_set>
#include <string>
#include "ModelerModule.h"
#include "NegativeFeatureFetcherInterface.h"

class ModelDataCleaner : public ModelerModule {

private:

public:

EntityToModuleStateStats* entityToModuleStateStats;

ModelDataCleaner();
virtual ~ModelDataCleaner();

void process(std::shared_ptr<ModelerContext> context);
std::string getName();
std::unordered_set<std::string> getCommonFeaturesBetweenPositiveAndNegativeDevices(std::shared_ptr<ModelerContext> context);
std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > improveQualityOfDeviceHistories(
        std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > deviceHistories);

void removeFeaturesFromHistory(
        std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > deviceHistories,
        std::unordered_set<std::string> commonFeaturesToRemove);

std::unordered_set<std::string>
getCommonDevicesBetweenPositiveAndNegativeDevices(std::shared_ptr<ModelerContext> context);

void removeDevicesFromHistory(
        std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > deviceHistories,
        std::unordered_set<std::string> commonDevicesToRemove);

std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > >
removeDevicesWithZeroFeatures(
        std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > deviceHistories);

void recordFeaturesUsed(std::shared_ptr<ModelerContext> context);

};

#endif /* ModelDataCleaner_H_ */
