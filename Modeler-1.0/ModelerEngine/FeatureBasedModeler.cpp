
#include "ModelRequest.h"
#include "EntityToModuleStateStats.h"
#include "CollectionUtil.h"
#include "SgdSolver.h"

#include "Device.h"

#include "GUtil.h"
#include "ModelRequest.h"
#include "FeatureValidator.h"
#include "FeatureWeightToModelConverter.h"
#include "FeatureWeightCalculator.h"
#include "DeviceFeatureHistory.h"

#include "ModelUtil.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureBasedModeler.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>


FeatureBasedModeler::FeatureBasedModeler() {
}


FeatureBasedModeler::~FeatureBasedModeler() {

}


void FeatureBasedModeler::process(std::shared_ptr<ModelerContext> context) {

								std::unordered_map<std::string, int> featureIndexMap;
								std::set<std::string> allDeviceFeatureSet;

								if (!areDeviceFeatureHistoriesValidForModeling(context)) {
																return;
								}

								for (auto&& oneDeviceHistory : *context->allDeviceHistories) {
																allDeviceFeatureSet =
																								ModelUtil::getFeatureSetFromDeviceHistories(oneDeviceHistory, context->modelRequest->featureTypesRequested);
																ModelUtil::updateFeatureIndexMapWithNewFeatures(allDeviceFeatureSet, featureIndexMap);
																MLOG(1)<<"collected "<<allDeviceFeatureSet.size() << " so far";
								}
								if(!context->allDeviceHistories->empty() && allDeviceFeatureSet.empty()) {
																context->redFlags.push_back(
																								"could not successfully get feature set from " +
																								_toStr(context->allDeviceHistories->size())
																								+ " device feature histories");
																return;
								}
								std::unordered_map<int, std::string> indexFeatureMap = ModelUtil::getIndexFeatureMap(featureIndexMap);

								std::vector<float64_t> featureWeights =
																featureWeightCalculator->caclulateWeights(
																								context,
																								featureIndexMap);

								MLOG(3)<<"features weights size  : "<<featureWeights.size();
								featureValidator->validateFeatures(featureWeights, featureIndexMap);

								featureWeightToModelConverter->createModelFromFeatureWeights(
																featureWeights,
																indexFeatureMap,
																context->modelRequest);

								context->modelRequest->modelResult->processResult = ModelResult::PROCESS_RESULT_CREATED;
								context->modelRequest->modelResult->redFlags.clear();
								MLOG(3)<<"this model was created : "<< context->modelRequest->modelResult->toJson()
															<< " for model request "<< context->modelRequest->toJson();
}

bool FeatureBasedModeler::areDeviceFeatureHistoriesValidForModeling(std::shared_ptr<ModelerContext> context) {
								if (context->positiveDeviceHistories->empty()) {
																entityToModuleStateStats->
																addStateModuleForEntity("ABORTING_MODELING_FOR_EMPTY_POSITIVES",
																																								"FeatureBasedModeler",
																																								"model" + StringUtil::toStr(context->modelRequest->id),
																																								EntityToModuleStateStats::exception);
																LOG(ERROR)<<"ABORTING_MODELING_FOR_EMPTY_POSITIVES";
																return false;
								}

								if ( context->negativeDeviceHistories->empty()) {
																entityToModuleStateStats->
																addStateModuleForEntity("ABORTING_MODELING_FOR_EMPTY_NEGATIVES",
																																								"FeatureBasedModeler",
																																								"model" + StringUtil::toStr(context->modelRequest->id),
																																								EntityToModuleStateStats::exception);
																LOG(ERROR)<<"ABORTING_MODELING_FOR_EMPTY_NEGATIVES";
																return false;
								}

								return true;
}



std::string FeatureBasedModeler::getName() {
								return "FeatureBasedModeler";
}
