/*
 * ModelDataProvider.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef ModelDataProvider_H_
#define ModelDataProvider_H_

class Device;
class ModelerContext;

class DeviceFeatureHistoryTestHelper;
class DeviceFeatureHistory;
class EntityToModuleStateStats;

#include "PositiveFeatureFetcherInterface.h"
#include "ModelerModule.h"
#include "NegativeFeatureFetcherInterface.h"

class ModelDataProvider : public ModelerModule {

private:
void provideDeviceHistoriesBasedOnPixelModel(std::shared_ptr<ModelerContext> context);
void provideDeviceHistoriesBasedOnSeedModel(std::shared_ptr<ModelerContext> context);

public:

std::shared_ptr<PositiveFeatureFetcherInterface>  positiveFeatureFetcher;
std::shared_ptr<NegativeFeatureFetcherInterface>  negativeFeatureFetcher;
EntityToModuleStateStats* entityToModuleStateStats;

ModelDataProvider(std::shared_ptr<NegativeFeatureFetcherInterface>  negativeFeatureFetcherArg,
                  std::shared_ptr<PositiveFeatureFetcherInterface>  positiveFeatureFetcherArg);
virtual ~ModelDataProvider();

void process(std::shared_ptr<ModelerContext> context);
std::string getName();

};

#endif /* ModelDataProvider_H_ */
