
#include "Device.h"

#include "GUtil.h"

#include "GUtil.h"
#include "DeviceFeatureHistory.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DeviceFeatureHistoryTestHelper.h"
#include "PositiveFeatureFetcherInterface.h"
#include "NegativeFeatureFetcherInterface.h"
#include "ModelerContext.h"
#include "ModelUtil.h"
#include "CollectionUtil.h"
#include "ModelDataProvider.h"
#include "ModelRequest.h"


ModelDataProvider::ModelDataProvider(
								std::shared_ptr<NegativeFeatureFetcherInterface> negativeFeatureFetcherArg,
								std::shared_ptr<PositiveFeatureFetcherInterface> positiveFeatureFetcherArg)  {

								if(!positiveFeatureFetcherArg || !negativeFeatureFetcherArg) {
																throwEx("feature fetchers are null");
								}
								this->negativeFeatureFetcher = negativeFeatureFetcherArg;
								this->positiveFeatureFetcher = positiveFeatureFetcherArg;
}

ModelDataProvider::~ModelDataProvider() {

}

void ModelDataProvider::process(std::shared_ptr<ModelerContext> context) {
								MLOG(3)<<"processing this modelRequestDto : "<<context->modelRequest->toJson();

								if (context->modelRequest->modelType.compare(ModelRequest::seedModel) == 0) {
																assertAndThrow(context->modelRequest->seedWebsites.size() != 0);
																provideDeviceHistoriesBasedOnSeedModel(context);

								} else if (context->modelRequest->modelType.compare(ModelRequest::pixelModel) == 0) {
																provideDeviceHistoriesBasedOnPixelModel(context);

								} else {
																throwEx("model type is wrong. type : " + context->modelRequest->modelType + ", request : "+ context->modelRequest->toJson());
								}

}

void ModelDataProvider::provideDeviceHistoriesBasedOnPixelModel(std::shared_ptr<ModelerContext> context) {

								positiveFeatureFetcher->fetchPositveFeaturesBasedOnPixelModel(context);

								MLOG(3)<<"positiveDeviceHistories size :  "<<
																context->positiveDeviceHistories->size()
															<<" for positiveOfferIds :"<<JsonArrayUtil::convertListToJson(
																CollectionUtil::convertSetToList<int>(
																								context->modelRequest->positiveOfferIds
																								));


								negativeFeatureFetcher->fetchNegativeFeaturesBasedOnPixelModel(context);

								MLOG(3)<<"negativeDeviceHistories size :  "<<
																context->negativeDeviceHistories->size()<<" for negativeOfferIds :"<<
																JsonArrayUtil::convertListToJson(
																CollectionUtil::convertSetToList<int>(context->modelRequest->negativeOfferIds));
}

void ModelDataProvider::provideDeviceHistoriesBasedOnSeedModel(
								std::shared_ptr<ModelerContext> context) {
								std::string seedWebsites = JsonArrayUtil::convertListToJson(
																CollectionUtil::convertSetToList<std::string>(
																								context->modelRequest->seedWebsites));

								this->positiveFeatureFetcher->
								fetchPositveFeaturesBasedOnSeedModel(context);

								MLOG(3)<<"positiveDeviceHistories size :  "
															<<context->positiveDeviceHistories->size()<<" for seedWebsites :"<<
																seedWebsites;

								negativeFeatureFetcher->
								fetchNegativeFeaturesBasedOnSeedModel(context);

								MLOG(3)<<"negativeDeviceHistories size :  "<<context->negativeDeviceHistories->size()
															<<" for seedWebsites :"<<seedWebsites;
}

std::string ModelDataProvider::getName() {
								return "ModelDataProvider";
}
