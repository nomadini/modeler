
#include "Device.h"
#include "DeviceFeatureHistory.h"
#include "GUtil.h"


#include "ModelUtil.h"
#include "SgdSolver.h"
#include "FeatureWeightCalculator.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "CollectionUtil.h"
#include "ModelUtil.h"


FeatureWeightCalculator::FeatureWeightCalculator()  {

}


FeatureWeightCalculator::~FeatureWeightCalculator()  {

}


std::vector<float64_t> FeatureWeightCalculator::caclulateWeights(
								std::shared_ptr<ModelerContext> context,
								std::unordered_map<std::string, int>& featureIndexMap) {
								auto allDeviceHistories = context->allDeviceHistories;
								//this should never throw, because we check before this step too
								assertAndThrow(!allDeviceHistories->empty());
								assertAndThrow(!featureIndexMap.empty());

								allDeviceHistories = CollectionUtil::shuffleTheList(
																allDeviceHistories);
								allDeviceHistories = CollectionUtil::shuffleTheList(
																allDeviceHistories);

								int dim_feat = featureIndexMap.size();
								int num_samples = allDeviceHistories->size();
								SGMatrix<float64_t> mat = SGMatrix<float64_t>(dim_feat, num_samples);
								SGVector<float64_t> labelVector(num_samples);

								MLOG(3)<<"Modeler : matrix created , dim_feat :  "<<
																dim_feat<<" , num_samples :  "<<
																num_samples;
								std::string allMaxtrixAsString;
								int numberOfPositiveLabels = 0;
								int numberOfNegativeLabels = 0;
								for (int32_t i = 0; i < num_samples; i++) {

																auto deviceHistory = allDeviceHistories->at(i);

																std::vector<int> allIndexes =
																								ModelUtil::getAllFeatureIndexesInDeviceFeatureHistory(
																																deviceHistory,
																																featureIndexMap);
																if (deviceHistory->positiveHistoryValue == true) {
																								labelVector[i] = 1.0;
																								numberOfPositiveLabels++;
																} else {
																								labelVector[i] = -1.0;
																								numberOfNegativeLabels++;
																}

																std::string rowContent;
																for (int32_t j = 0; j < dim_feat; j++) {
																								if (CollectionUtil::valueExistInList(allIndexes, j)) {
																																mat(j, i) = 1;
																								} else {
																																mat(j, i) = 0;
																								}
																								rowContent += StringUtil::toStr(mat(j, i));
																								allMaxtrixAsString += rowContent;
																}

								}

								MLOG(1) << "this is matrix of data for model \n "<<allMaxtrixAsString;
								//create train labels
								if (numberOfNegativeLabels == 0 || numberOfPositiveLabels == 0) {
																context->redFlags.push_back(
																								"cannot build models with non 2 lables"
																								", numberOfPositiveLabels : " +
																								_toStr(numberOfPositiveLabels) +
																								" , numberOfNegativeLabels "
																								+ _toStr(numberOfNegativeLabels));
																std::vector<float64_t> noScores;
																return noScores;
								}



								CBinaryLabels * labels = new CBinaryLabels(labelVector);
								CDotFeatures * features = new CDenseFeatures<float64_t>(mat);
								SG_REF(features);
								SG_REF(labels);

								MLOG(3)<<"about to process svm sgd";
								auto sgdSolver = std::make_shared<SgdSolver>();
								sgdSolver->entityToModuleStateStats = entityToModuleStateStats;
								std::vector<float64_t> featureWeights =
																sgdSolver->createSvmSgdModel(features, labels);
								MLOG(3)<<"feature weights created.. with size : "<<featureWeights.size();
								delete features;
								delete labels;

								return featureWeights;
}
