/*
 * FeatureWeightCalculator.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef FeatureWeightCalculator_H_
#define FeatureWeightCalculator_H_

class Device;
class DeviceFeatureHistory;
#include <shogun/lib/common.h>
class EntityToModuleStateStats;
class SgdSolver;

#include "ModelerModule.h"

class FeatureWeightCalculator {

public:
EntityToModuleStateStats* entityToModuleStateStats;

FeatureWeightCalculator();
virtual ~FeatureWeightCalculator();
/*
 * creates a feature matrix and label matrix in memory based on
 * positive and negative Devicehistories for svmSgd
 */

std::vector<float64_t> caclulateWeights(
        std::shared_ptr<ModelerContext> context,
        std::unordered_map<std::string, int>& featureIndexMap);


void process(std::shared_ptr<ModelerContext> context);

std::string getName();
};

#endif
