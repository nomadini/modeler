/*
 * ModelerPersistenceService.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef MODELER_PERSISTENCE_SERVICE_H_
#define MODELER_PERSISTENCE_SERVICE_H_

#include "gmock/gmock.h" //move me to a different cpp file
class EntityToModuleStateStats;
class MySqlModelService;
class ModelerPersistenceService;
class MySqlModelResultService;
class ModelRequest;
#include "ModelerModule.h"

class ModelerPersistenceService : public ModelerModule {

public:
EntityToModuleStateStats* entityToModuleStateStats;
MySqlModelService* mySqlModelService;
MySqlModelResultService* mySqlModelResultService;

ModelerPersistenceService();

void process(std::shared_ptr<ModelerContext> context);
void validateFeatureTypesRequested(std::shared_ptr<ModelRequest> model);

std::string getName();
};


class ModelerPersistenceServiceMock;



class ModelerPersistenceServiceMock : public ModelerPersistenceService {

public:

ModelerPersistenceServiceMock() {
}
// MOCK_METHOD0(PenUp, void());
//  MOCK_METHOD0(PenDown, void());
//  MOCK_METHOD1(Forward, void(int distance));
//  MOCK_METHOD1(Turn, void(int degrees));
//  MOCK_METHOD2(GoTo, void(int x, int y));
//  MOCK_CONST_METHOD0(GetX, int());
//  MOCK_CONST_METHOD0(GetY, int());

MOCK_METHOD1(persistModelInDataStores, void(std::shared_ptr<ModelRequest>
                                            model));
//    MOCK_METHOD1(persistModelInDataStores, void(int num));
};

#endif /* MODELER_PERSISTENCE_SERVICE_H_ */
