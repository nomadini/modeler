#include "Device.h"
#include "GUtil.h"


#include "ModelRequest.h"
#include "ModelerPersistenceService.h"
#include "MySqlDriver.h"
#include "MySqlModelService.h"
#include "CassandraDriverInterface.h"
#include "ModelerPersistenceService.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlModelResultService.h"
#include "MySqlDriver.h"
#include "ModelRequest.h"
#include "ModelerPersistenceService.h"
#include "MySqlModelService.h"
ModelerPersistenceService::ModelerPersistenceService() {
}



void ModelerPersistenceService::process(std::shared_ptr<ModelerContext> context) {
        LOG(INFO)<<"going to persist the nicely built model : "<<context->modelRequest->toshortString()
                 <<"\nmodel result is : "<<context->modelRequest->modelResult->toJson();
        context->modelRequest->rebuildModelOnNextRun = false;
        validateFeatureTypesRequested(context->modelRequest);
        this->mySqlModelService->upsertModel (context->modelRequest);
        context->modelRequest->modelResult->modelRequestId = context->modelRequest->id;
        this->mySqlModelResultService->upsertModel (context->modelRequest->modelResult);
}

void ModelerPersistenceService::validateFeatureTypesRequested(std::shared_ptr<ModelRequest> model) {

        // for (mode->)
}

std::string ModelerPersistenceService::getName() {
        return "ModelerPersistenceService";
}
