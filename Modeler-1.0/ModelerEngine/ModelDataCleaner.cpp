
#include "Device.h"

#include "GUtil.h"

#include "GUtil.h"
#include "DeviceFeatureHistory.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DeviceFeatureHistoryTestHelper.h"
#include "PositiveFeatureFetcherInterface.h"
#include "NegativeFeatureFetcherInterface.h"
#include "ModelerContext.h"
#include "ModelUtil.h"
#include "CollectionUtil.h"
#include "ModelDataCleaner.h"
#include "ModelRequest.h"


ModelDataCleaner::ModelDataCleaner()  {

}

ModelDataCleaner::~ModelDataCleaner() {

}


void ModelDataCleaner::process(std::shared_ptr<ModelerContext> context) {

								context->positiveDeviceHistories =
																improveQualityOfDeviceHistories(context->positiveDeviceHistories);
								LOG(INFO)<< " context->positiveDeviceHistories size after weeding out too spartse features : "
																	<< context->positiveDeviceHistories->size();
								context->negativeDeviceHistories =
																improveQualityOfDeviceHistories(context->negativeDeviceHistories);
								LOG(INFO)<< " negativeDeviceHistories size after weeding out too spartse features : "
																	<< context->negativeDeviceHistories->size();


								auto commonFeaturesToRemove = getCommonFeaturesBetweenPositiveAndNegativeDevices(context);

								LOG(INFO)<< "size of common features to remove size  : "
																	<< commonFeaturesToRemove.size();

								removeFeaturesFromHistory(context->positiveDeviceHistories, commonFeaturesToRemove);
								removeFeaturesFromHistory(context->negativeDeviceHistories, commonFeaturesToRemove);

								context->positiveDeviceHistories = removeDevicesWithZeroFeatures(context->positiveDeviceHistories);
								context->negativeDeviceHistories = removeDevicesWithZeroFeatures(context->negativeDeviceHistories);


								auto commonDevicesToRemove = getCommonDevicesBetweenPositiveAndNegativeDevices(context);
								LOG(INFO)<< "size of common devices to remove size  : "
																	<< commonDevicesToRemove.size();


								LOG(INFO)<< "size of positive devices before removing commons  : "<<context->positiveDeviceHistories->size();
								removeDevicesFromHistory(context->positiveDeviceHistories, commonDevicesToRemove);
								LOG(INFO)<< "size of positive devices after removing commons  : "<<context->positiveDeviceHistories->size();

								LOG(INFO)<< "size of negative devices before removing commons  : "<<context->negativeDeviceHistories->size();
								removeDevicesFromHistory(context->negativeDeviceHistories, commonDevicesToRemove);
								LOG(INFO)<< "size of negative devices after removing commons  : "<<context->negativeDeviceHistories->size();


								//combine devices into one group
								context->allDeviceHistories = CollectionUtil::combineTwoLists<
																std::shared_ptr<DeviceFeatureHistory> >(
																context->positiveDeviceHistories,
																context->negativeDeviceHistories);


								recordFeaturesUsed(context);
								LOG(INFO)<< "size of all devices after cleaning  : "<<context->allDeviceHistories->size();
}


std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > >
ModelDataCleaner::removeDevicesWithZeroFeatures(
								std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > deviceHistories) {
								auto newDeviceHistories =
																std::make_shared<std::vector<std::shared_ptr<DeviceFeatureHistory> > > ();

								for(auto dh : *deviceHistories) {
																if (!dh->getFeatures()->empty()) {
																								newDeviceHistories->push_back(dh);
																}
								}

								return newDeviceHistories;
}

std::unordered_set<std::string> ModelDataCleaner::
getCommonDevicesBetweenPositiveAndNegativeDevices(std::shared_ptr<ModelerContext> context) {
								auto positiveDeviceIdsMap =
																std::make_shared<std::unordered_map<std::string, std::string> > ();
								auto negativeDeviceIdsMap =
																std::make_shared<std::unordered_map<std::string, std::string> >();
								for(auto posFeatureDeviceHistory : *context->positiveDeviceHistories) {
																positiveDeviceIdsMap->insert(
																								std::make_pair(
																																posFeatureDeviceHistory->device->getDeviceUniqueName(),
																																posFeatureDeviceHistory->device->getDeviceUniqueName()));

								}
								for(auto negativeFeatureDeviceHistory : *context->negativeDeviceHistories) {
																negativeDeviceIdsMap->insert(
																								std::make_pair(
																																negativeFeatureDeviceHistory->device->getDeviceUniqueName(),
																																negativeFeatureDeviceHistory->device->getDeviceUniqueName()));

								}

								auto commonKeys = CollectionUtil::getCommonKeysOfMaps(*negativeDeviceIdsMap, *positiveDeviceIdsMap);
								MLOG(1) << " found "<< commonKeys.size() << " common devices between positive and negative devices";
								return commonKeys;
}

std::unordered_set<std::string> ModelDataCleaner::
getCommonFeaturesBetweenPositiveAndNegativeDevices(std::shared_ptr<ModelerContext> context) {
								std::unordered_set<std::string> commonFeaturesToRemove;
								auto positiveFeatuesMap =
																std::make_shared<std::unordered_map<std::string, std::string> > ();
								auto negativeFeatuesMap =
																std::make_shared<std::unordered_map<std::string, std::string> >();
								//build positiveFeatuesMap
								for(auto posFeatureDeviceHistory : *context->positiveDeviceHistories) {
																for(auto fh : *posFeatureDeviceHistory->getFeatures()) {
																								positiveFeatuesMap->insert(std::make_pair(fh->feature->name, fh->feature->name));
																}

								}

								//build negativeFeatuesMap
								for(auto negativeDeviceHistory : *context->negativeDeviceHistories) {
																for(auto fh : *negativeDeviceHistory->getFeatures()) {
																								negativeFeatuesMap->insert(std::make_pair(fh->feature->name, fh->feature->name));
																}
								}

								commonFeaturesToRemove = CollectionUtil::getCommonKeysOfMaps(*negativeFeatuesMap, *positiveFeatuesMap);

								MLOG(1) << " found "<< commonFeaturesToRemove.size() << " common features between positive and negative devices";
								return commonFeaturesToRemove;
}

void ModelDataCleaner::removeDevicesFromHistory(
								std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > deviceHistories,
								std::unordered_set<std::string> commonDevicesToRemove) {

								if(commonDevicesToRemove.empty()) {
																return;
								}

								auto cleanDevices = std::make_shared<std::vector<std::shared_ptr<DeviceFeatureHistory> > >();

								auto deviceUniqueNameMap =
																std::make_shared<
																								std::unordered_map<std::string, std::shared_ptr<DeviceFeatureHistory> > > ();
								for(auto deviceFeatureHistory : *deviceHistories) {
																deviceUniqueNameMap->insert(
																								std::make_pair(
																																deviceFeatureHistory->device->getDeviceUniqueName(),
																																deviceFeatureHistory));

								}
								// as as
								for (auto deviceToRemove : commonDevicesToRemove) {
																auto pair = deviceUniqueNameMap->find(deviceToRemove);
																if ( pair !=
																					deviceUniqueNameMap->end()) {
																								//we can add this device , because it's not part of keys to remove

																								cleanDevices->push_back(pair->second);
																}
								}


								deviceHistories = cleanDevices;
}

void ModelDataCleaner::removeFeaturesFromHistory(
								std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > deviceHistories,
								std::unordered_set<std::string> commonFeaturesToRemove) {
								if(commonFeaturesToRemove.empty()) {
																return;
								}
								for (auto deviceFeatureHistory : *deviceHistories) {
																//then we clean all the common features between positive and negative devices
																int sizeOfFeaturesBeforeRemove = deviceFeatureHistory->getFeatures()->size();
																deviceFeatureHistory->removeFeaturesFromHistory(commonFeaturesToRemove);
																MLOG(2)<< "sizeOfFeatures in deviceFeatureHistory after removing common features : "
																							<< deviceFeatureHistory->getFeatures()->size()
																							<<", sizeOfFeaturesBeforeRemove : "
																							<<sizeOfFeaturesBeforeRemove;
								}

}

std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > >
ModelDataCleaner::improveQualityOfDeviceHistories(
								std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > deviceHistories) {
								auto cleanDeviceFeatureHistories =
																std::make_shared<std::vector<std::shared_ptr<DeviceFeatureHistory> > >();

								for (auto originalDeviceHistory : *deviceHistories) {
																//here we clean out the features that were not seen enough times
																auto cleanDevHist = originalDeviceHistory->getCleanHistoryClone(2);
																if (cleanDevHist->getFeatures()->empty()) {
																								if(originalDeviceHistory->getFeatures()->size() <=10) {
																																//original device history is small, so we use it
																																cleanDeviceFeatureHistories->push_back(originalDeviceHistory);
																								}
																								LOG(WARNING) <<"device doesn't have any clean features, it had "
																																					<<originalDeviceHistory->getFeatures()->size() << " features";
																} else {
																								cleanDeviceFeatureHistories->push_back(cleanDevHist);
																}
								}

								return cleanDeviceFeatureHistories;

}

void ModelDataCleaner::recordFeaturesUsed(std::shared_ptr<ModelerContext> context) {

								for (auto dh : *context->positiveDeviceHistories) {
																for(auto fh : *dh->getFeatures()) {
																								context->modelRequest->modelResult->positiveFeaturesUsed.insert(
																																fh->feature->getName());
																}
								}

								if (context->modelRequest->modelResult->positiveFeaturesUsed.empty()) {
																std::string msg = "unable to fetch positive features from " +
																																		_toStr(context->positiveDeviceHistories->size())+ " device histories";
																context->redFlags.push_back(msg);
								}



								for (auto dh : *context->negativeDeviceHistories) {
																for(auto fh : *dh->getFeatures()) {
																								context->modelRequest->modelResult->
																								negativeFeaturesUsed.insert(
																																fh->feature->getName());
																}
								}

								if (context->modelRequest->modelResult->negativeFeaturesUsed.empty()) {
																std::string msg =
																								"unable to fetch negative features from " +
																								_toStr(context->modelRequest->modelResult->negativeFeaturesUsed.size())+ "device histories";
																context->redFlags.push_back(msg);
								}

}

std::string ModelDataCleaner::getName() {
								return "ModelDataCleaner";
}
