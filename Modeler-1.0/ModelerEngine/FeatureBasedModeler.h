/*
 * FeatureBasedModeler.h
 *
 *  Created on: Jul 16, 2015
 *      Author: mtaabodi
 */

#ifndef SVMSGDMODELER_H_
#define SVMSGDMODELER_H_

class FeatureWeightToModelConverter;
class FeatureWeightToModelConverter;
class ModelRequest;
class ModelerPersistenceService;
class EntityToModuleStateStats;

class ModelerContext;
class FeatureValidator;
class DeviceFeatureHistory;

#include "ModelerModule.h"
#include "FeatureWeightCalculator.h"

class FeatureBasedModeler : public ModelerModule {

public:

EntityToModuleStateStats* entityToModuleStateStats;

std::shared_ptr<FeatureValidator> featureValidator;
std::shared_ptr<FeatureWeightToModelConverter> featureWeightToModelConverter;
std::shared_ptr<FeatureWeightCalculator > featureWeightCalculator;

FeatureBasedModeler();

virtual ~FeatureBasedModeler();

void process(std::shared_ptr<ModelerContext> context);
bool areDeviceFeatureHistoriesValidForModeling(std::shared_ptr<ModelerContext> context);
std::string getName();

};

#endif /* SVMSGDMODELER_H_ */
