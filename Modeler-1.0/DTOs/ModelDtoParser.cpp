
#include "GUtil.h"

#include "ModelRequest.h"
#include "ModelRequest.h"
#include "ModelDtoParser.h"


#include "JsonUtil.h"
#include "GUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
using namespace std;

std::string ModelDtoParser::determineTheModelType(std::string requestBody) {

        MLOG(3)<<"requestBody  "<<requestBody;
        auto document = parseJsonSafely (requestBody);

        if (document->HasMember ("modelType")) {
                return (*document)["modelType"].GetString ();
        }
        throwEx ("modelType is required in model requests");
}
