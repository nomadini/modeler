//
// Created by Mahmoud Taabodi on 11/19/15.
//

#ifndef GICAPODS_ModelDtoParser_H
#define GICAPODS_ModelDtoParser_H


#include <memory>
#include <string>
#include <vector>

class ModelDtoParser {

private:

public:

static std::string determineTheModelType(std::string requestBody);
};

#endif //GICAPODS_MODEL_DTO_UTIL_H
