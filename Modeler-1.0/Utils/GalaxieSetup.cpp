
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "CollectionUtil.h"
#include "SignalHandler.h"
#include "GalaxieSetup.h"

void GalaxieSetup::setup() {
        shogun::init_shogun_with_defaults();
        std::set_terminate (gicapods::Util::myterminate);
        SignalHandler::installHandlers();

}
