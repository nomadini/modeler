#ifndef ModelConstants_h
#define ModelConstants_h






class ModelConstants {

public:

    static constexpr const char* defaultPlanInJson= "[ {\"8\":30}, {\"12\":50},  {\"18\":80}  , {\"22\":100} ]";
    static constexpr const char* logFileName = "/var/data/log/mylogFile";
    static constexpr const char* bidEventLogFileName =  "/var/data/log/bidEventLogFile";

};
#endif
