
#include "GUtil.h"
#include "PixelDeviceHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PixelModelNegativeFeatureFetcher.h"
#include "MySqlOfferService.h"
#include "DataReloadService.h"
#include "CassandraDriverInterface.h"
#include "ConfigService.h"
#include "TempUtil.h"
#include "LogLevelManager.h"
#include <thread>
#include "GUtil.h"
#include "CacheService.h"
#include "StringUtil.h"
#include "SignalHandler.h"
#include <string>
#include <memory>
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "ConfigService.h"
#include "StringUtil.h"
#include <string>
#include "EntityToModuleStateStatsPersistenceService.h"
#include "MySqlMetricService.h"
#include "ModelerAsyncJobService.h"
#include "FeatureBasedModeler.h"
#include "AtomicBoolean.h"
#include "ModelRequest.h"
#include "MySqlPixelService.h"
#include "FeatureFetcherService.h"
#include "Poco/NotificationQueue.h"
#include "Pixel.h"
#include "AsyncThreadPoolService.h"

#include <shogun/lib/config.h>
#include <shogun/base/init.h>
#include "BeanFactory.h"
#include "ModelRequest.h"
#include "ModelerPersistenceService.h"
#include "ModelDataCleaner.h"
#include "FeatureValidator.h"
#include "FeatureWeightToModelConverter.h"
#include "FeatureWeightCalculator.h"
#include "NegativeFeatureFetcher.h"
#include "DeviceFeatureHistory.h"
#include "PositiveFeatureFetcher.h"
#include "ServiceFactory.h"
#include "FeatureDeviceHistory.h"

int main(int argc, char* argv[]) {

        std::string appName = "Modeler";
        TempUtil::configureLogging (appName, argv);
        std::string propertyFileName = "modeler.properties";
        auto beanFactory = std::make_unique<BeanFactory>("SNAPSHOT");
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->commonPropertyFileName = "common.properties";
        beanFactory->appName = appName;
        beanFactory->initializeModules();
        auto serviceFactory = std::make_unique<ServiceFactory>(beanFactory.get());
        serviceFactory->initializeModules();

        LOG(INFO) << "google log starting the Modeler : argc "<< argc << " argv : " <<  *argv;
        auto isMetricPersistenceInProgress = std::make_shared<gicapods::AtomicBoolean>(false);


        auto modelerPersistenceService = std::make_shared<ModelerPersistenceService> ();
        modelerPersistenceService->mySqlModelService = beanFactory->mySqlModelService.get();
        modelerPersistenceService->mySqlModelResultService = beanFactory->mySqlModelResultService.get();

        modelerPersistenceService->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();



        LOG(INFO)<<"about to build model";
        auto featureFetcherService = std::make_shared<FeatureFetcherService>(
                beanFactory->pixelDeviceHistoryCassandraService.get(),
                (CacheService<Offer>*)beanFactory->offerCacheService.get(),
                beanFactory->deviceFeatureHistoryCassandraService.get(),
                beanFactory->featureDeviceHistoryCassandraService.get());

        featureFetcherService->pixelCacheService = beanFactory->pixelCacheService.get();
        featureFetcherService->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        featureFetcherService->offerPixelMapCacheService = beanFactory->offerPixelMapCacheService.get();


        auto pixelModelNegativeFeatureFetcher = std::make_shared<PixelModelNegativeFeatureFetcher>(featureFetcherService);
        pixelModelNegativeFeatureFetcher->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        pixelModelNegativeFeatureFetcher->deviceFeatureHistoryCassandraService = beanFactory->deviceFeatureHistoryCassandraService.get();
        pixelModelNegativeFeatureFetcher->featureDeviceHistoryCassandraService = beanFactory->featureDeviceHistoryCassandraService.get();

        auto negativeFeatureFetcher = std::make_shared<NegativeFeatureFetcher>();
        negativeFeatureFetcher->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        negativeFeatureFetcher->deviceFeatureHistoryCassandraService = beanFactory->deviceFeatureHistoryCassandraService.get();
        negativeFeatureFetcher->featureDeviceHistoryCassandraService = beanFactory->featureDeviceHistoryCassandraService.get();
        negativeFeatureFetcher->pixelModelNegativeFeatureFetcher = pixelModelNegativeFeatureFetcher.get();

        auto positiveFeatureFetcher = std::make_shared<PositiveFeatureFetcher>(featureFetcherService);
        positiveFeatureFetcher->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        positiveFeatureFetcher->deviceFeatureHistoryCassandraService = beanFactory->deviceFeatureHistoryCassandraService.get();
        positiveFeatureFetcher->featureDeviceHistoryCassandraService = beanFactory->featureDeviceHistoryCassandraService.get();



        auto modelDataCleaner = std::make_shared<ModelDataCleaner>();
        modelDataCleaner->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();


        auto featureValidator = std::make_shared<FeatureValidator>();
        auto featureWeightToModelConverter = std::make_shared<FeatureWeightToModelConverter>();
        auto featureWeightCalculator = std::make_shared<FeatureWeightCalculator>();
        featureWeightCalculator->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();

        auto featureBasedModeler = std::make_shared<FeatureBasedModeler>();
        featureBasedModeler->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        featureBasedModeler->featureWeightCalculator = featureWeightCalculator;
        featureBasedModeler->featureWeightToModelConverter = featureWeightToModelConverter;
        featureBasedModeler->featureValidator = featureValidator;

        auto modelerAsyncJobService = std::make_shared<ModelerAsyncJobService>();
        modelerAsyncJobService->modelCacheService = beanFactory->modelCacheService.get();
        modelerAsyncJobService->modelResultCacheService = beanFactory->modelResultCacheService.get();
        modelerAsyncJobService->configService = beanFactory->configService.get();
        modelerAsyncJobService->negativeFeatureFetcher = negativeFeatureFetcher.get();
        modelerAsyncJobService->positiveFeatureFetcher = positiveFeatureFetcher.get();
        modelerAsyncJobService->modelDataCleaner = modelDataCleaner.get();
        modelerAsyncJobService->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        modelerAsyncJobService->isMetricPersistenceInProgress = isMetricPersistenceInProgress;
        modelerAsyncJobService->entityToModuleStateStatsPersistenceService =
                beanFactory->entityToModuleStateStatsPersistenceService.get();

        modelerAsyncJobService->dataReloadService = serviceFactory->dataReloadService.get();
        modelerAsyncJobService->modelerPersistenceService = modelerPersistenceService.get();
        modelerAsyncJobService->featureBasedModeler = featureBasedModeler.get();
        modelerAsyncJobService->startOtherThreads();

        std::thread asyncModeler (&ModelerAsyncJobService::runAsynchronousModeler, modelerAsyncJobService.get());
        asyncModeler.join ();

        beanFactory->cassandraDriver->closeSessionAndCluster();

}
